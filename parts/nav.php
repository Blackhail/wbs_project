<?php declare( strict_types = 1 );
$sitename = $_GET['select'] ?? NULL;
?>

<nav>
    <div class="nav-main burger clear">
        <input type="checkbox" id="hamburger">
        <label for="hamburger" class="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </label>
        <ul>
            <li><a class="<?php (($sitename == "Animation") ? "active" : "")?>" href="<?= url('overview.php?select=Animation&subselect=0') ?>">Animation</a></li>
            <li><a class="<?php ($sitename == "Design" ? "active" : "")?>" href="<?= url('overview.php?select=Design') ?>">Design</a></li>
            <li><a class="<?php ($sitename == "Interactive" ? "active" : "")?>" href="<?= url('overview.php?select=Interactive') ?>">Interactive</a></li>
            <li><a class="<?php ($sitename == "Artists" ? "active" : "")?>" href="<?= url('overview.php?select=Artists') ?>">Artists</a></li>
            <li><a class="<?php ($sitename == "About" ? "active" : "")?>" href="<?= url('about.php') ?>">About</a></li>
            <li><a class="<?php ($sitename == "News" ? "active" : "")?>" href="<?= url('news.php') ?>">News</a></li>
        </ul>
    </div>
</nav>

<div class="nav-main">
<div class="header-logo"><a href="<?= url('index.php') ?>"><img src="<?= url('../images/logo/grimm_logo_gray.jpg') ?>" alt="grimm logo"></a></div>
</div>

<?php 
$sitename = $_GET['select'] ?? NULL;

?>