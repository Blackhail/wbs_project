<?php declare( strict_types = 1 );

$sql_profile = "SELECT * FROM `users`";
$profiles = db_raw_select($sql_profile);

?>
<div id="wrapper">
<div class="login-pannel">

    <?php if (!auth_id()) : ?>
        <a href="<?= url('auth/login.php') ?>">Login</a>
    <?php endif; ?>

    <?php if (auth_id()) : ?>
        <a href="<?= url('auth/logout.php') ?>">Logout</a>
    <?php endif; ?>
   
    <?php foreach ($profiles as $profile) {
        if (auth_id() === $profile['id'] && $profile['admin']) : ?>
        <a href="<?= url('auth/register.php') ?>">Register</a>
    <?php endif; 
    } ?>
    
</div>