<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex">
    <!-- STYLES -->
    <link rel="stylesheet" href="<?= url('../styles/login.css') ?>">
    <link rel="stylesheet" href="<?= url('../styles/fonts.css') ?>">
    <link rel="stylesheet" href="<?= url('../styles/nav.css') ?>">
    <link rel="stylesheet" href="<?= url('../styles/main.css') ?>">
    <link rel="stylesheet" href="<?= url('../styles/forms.css') ?>">
    <link rel="stylesheet" href="<?= url('../styles/footer.css') ?>">
    <!-- SCRIPTS -->
    <script src="<?= url('../lib/js/html5shiv.min.js') ?>"></script>
    <script src="<?= url('../lib/js/repositories/helpers.js') ?>"></script>
    <script src="<?= url('../lib/js/repositories/polyfills.js') ?>"></script>
    <script src="<?= url('../lib/js/jquery/jquery-3.5.1.min.js') ?>"></script>
    <script src="<?= url('../lib/js/jquery/validate/jquery.validate.min.js') ?>"></script>
    <script src="<?= url('../lib/js/jquery/validate/additional-methods.min.js') ?>"></script>
    
    
    <title><?=$site_name?></title>
</head>
<body>
<noscript>
    Bitte aktivieren Sie JavaScript in Ihrem Browser - diese Seite funktioniert nur mit aktiviertem JavaScript.
</noscript>
<?php
    include PATH.'parts/login_pannel.php';
    include PATH.'parts/nav.php';
?>