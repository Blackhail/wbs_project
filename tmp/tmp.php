<html>
<head>
    <title>DateiUpload</title>
    <meta charset="utf-8">
</head>
<body>

    <form action="upload.php" method="post" enctype="multipart/form-data">
        <label for="projectname"></label>
        <input type="text" name="projectname" id="projectname">
        <textarea name="projectdescription" id="projectdescription" cols="30" rows="10"></textarea>
        <input type="submit" name="submit" value="send">
    </form>

    <form action="upload.php" method="post" enctype="multipart/form-data">
        <input type="file" name="datei">
        <input type="submit" name="submit" value="Datei hochladen">
    </form>

    <form action="upload.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" multiple>
        <input type="submit" name="submit" value="Datei hochladen">
    </form>

</body>
</html>

<!-- /////////////////////////////////////// -->

<?php

if (isset($_POST['submit'])) {

    # Informationen über die Datei erfahren
    $file = $_FILES['datei'];
    # Dateiname:
    $fileName = $_FILES['datei']['name'];
    # Dateigröße:
    $fileSize = $_FILES['datei']['size'];
    # DateiType
    $fileType = $_FILES['datei']['type'];
    # temporärer Speicherort der Datei:
    $fileTmpName = $_FILES['datei']['tmp_name'];
    # Error (hochgeladen: 0; nicht hochgeladen: 1)
    $fileError = $_FILES['datei']['error'];

    # Es sollen nur Bilder hochladen werden

    # Teilt den Inhalt der Variable fileName in einen
    # Teil vor dem Punkt und einen danach auf
    $fileExt = explode('.', $fileName);
    # strtolower besteht darauf, das der Inhalt in
    # Kleinbuchstaben geschrieben ist, also nicht JPG
    # end greift in dem oben erzeugten Array auf
    # den letzten Teil zu
    $fileActualExt = strtolower(end($fileExt));

    # Welche Dateien sollen hochgeladen werden dürfen?
    $allowed = array('jpg', 'jpeg', 'png', 'gif');

    # Prüfen, ob die hochzuladende Datei auch erlaubt ist:
    # Geprüft wird, ob $fileActualExt in dem Array $allowed
    # vorhanden ist
    if (in_array($fileActualExt, $allowed)){

        # Liegt ein Error vor
        if ($fileError === 0) {

            # Maximale Dateigröße festlegen (hier 500kb)
            if($fileSize < 500000) {

            # Sicherstellen, dass eine Datei mit einem
            # vorhandenen Namen nicht überschrieben wird,
            # indem die hochgeladene Datei umbenannt wird.
            $fileNameNew = uniqid('', true).".".$fileActualExt;

            # Der Zielort wird festgelegt:
            $fileDesitination = 'upload/'.$fileNameNew;
            # Die Datei wird vom tempäreren Speicherort in             # Ordner verschoben wird.
          move_uploaded_file($fileTmpName, $fileDesitination);

            # Ausgabe der Erfolgsmeldung auf der Uploadseite
            header("Location: index.php?uploadsuccess");

            } else {
                echo "Die Datei überschreitet die max. Größe";
            }

        } else {
            echo "Beim Hochladen ist ein Fehler aufgetreten";
        }

    } else {
        echo "Dieser Dateityp ist nicht erlaubt";
    }
}