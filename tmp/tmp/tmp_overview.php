<?php declare( strict_types = 1 );

$site_name = 'overview'; /* hier später generierter artist name*/

require_once '../bootstrap.php';

///// SQL ABFRAGEN ///////////////////////////////////////////////////////////////////////////

original:
$sql = "SELECT `id` , `name` , `artist_category` , `avatar` , `admin` FROM `users`";
$profiles = db_raw_select($sql);

$sql = "SELECT * FROM `project` , `media` WHERE  `media`.`project_id` = `project`.`id` ";
$projects = db_raw_select($sql);

///// GET VARIABLEN ///////////////////////////////////////////////////////////////////////////

$get_select = $_GET['select'] ?? NULL;
$get_subselect = $_GET['subselect'] ?? NULL;

/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>


<main class="card-box">

<!-- ///// CATEGORY LINKS ///////////////////////////////////////////////////////////////////////////////////////////////// -->

    <div>
        <!-- Überschrift aus linktext in Navi holen -->
        <h1>Artists</h1>
        <?php
        if ( $get_select == 'Artists' || $get_subselect == 'Animation' || $get_subselect == 'Design' ||$get_subselect == 'Interactive' ) { ?>
        <ul>
            <li><a href="<?= url('overview.php?subselect=Animation') ?> " title="Animation">Animation</a></li>
            <li><a href="<?= url('overview.php?subselect=Design') ?> "title="Design">Design</a></li>
            <li><a href="<?= url('overview.php?subselect=Interactive') ?> " title="Interactiv">Interactive</a></li>
        </ul>
        <?php
        }?>

    </div>

    <div class="cards">

<!-- ///// CARDS WITH CATEGORY //////////////////////////////////////////////////////////////////////////////////////////// -->

        <!-- click 4 in Mainnavi 'Artists' -->
        <?php 
        foreach ($profiles as $profile) {
            
            if ($get_select === 'Artists' && $profile['name'] !== 'admin') { 
            // var_dump($get_select);
            ?>
                <div class="card">
                    <figure>
                        <a href="profile_page.php?id=<?= $profile['id']?>"><img src="<?= '../' . $profile['avatar'] ?>" alt="profile-img"></a>
                        <figcaption><?= $profile['name'] ?></figcaption>
                    </figure>
                </div>
                            
            <?php
            }
        }?>

        <!-- click Artists Subtnavi -->
        <?php 
        foreach ($profiles as $profile) { 

            if ($get_subselect === $profile['artist_category'] && !$profile['admin']) {
            ?>
                <div class="card">
                    <figure>
                        <a href="profile_page.php?id=<?= $profile['id']?>"><img src="<?= '../' . $profile['avatar'] ?>" alt="profile-img"></a>
                        <figcaption><?= $profile['name'] ?></figcaption>
                    </figure>
                </div>
                            
            <?php
            }
        }?>

        <!-- click 1 - 3 in Mainnavi  -->
        <?php 
        foreach ($projects as $project) { 

            if ($get_select === $project['project_category']) { 
                if ($project['header_image']) { ?>
                <div class="card">
                    <figure>
                        <a href="project_page.php?project_id=<?= $project['project_id']?>"><img src="<?= '../' . $project['url'] ?>" alt="project-img"></a>
                        <figcaption><?= $project['name'] ?></figcaption>
                    </figure>
                </div>
                <?php
                }
            }    
        }
        ?>

        

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
    </div>
</main>



<?php
    include PATH.'parts/footer.php';