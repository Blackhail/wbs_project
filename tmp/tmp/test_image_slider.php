<?php declare( strict_types = 1 );

$site_name = 'grimm berlin';

require_once '../../bootstrap.php';


/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>


<main>
	<div class="slider">
        <div class="inner">
            <img src="<?= url('../images/user/jan/our_man_in_nirvana/visual4_ourmaninnirvana.jpg') ?>" alt="Alt 1" />
            <img src="<?= url('../images/user/jan/our_man_in_nirvana/visual1_ourmaninnirvana.jpg') ?>" alt="Alt 2" />
            <img src="<?= url('../images/user/jan/our_man_in_nirvana/visual3_ourmaninnirvana.jpg') ?>" alt="Alt 3" />
            <img src="<?= url('../images/user/jan/our_man_in_nirvana/visual2_ourmaninnirvana.jpg') ?>" alt="Alt 4" />
            <img src="<?= url('../images/user/jan/our_man_in_nirvana/visual5_ourmaninnirvana.jpg') ?>" alt="Alt 1" />
        </div>
	</div>
    
    <div class="slide-info clear">
        <p>Jan Köster</p>
        <p>Our Man in Nirvana</p>
    </div>
</main>
<script>
	(function(){
	    let left = 0
	    let cont = 1
	    const imagesToShow = parseInt($(".inner *").length)
	    setInterval(function() {
            if (cont++ <= imagesToShow){
                $(".inner").css("left",`${left}%`);
                left -= 100.3;
                } else{
                cont = 1
                left = 0
                }
	    },1500)
	})()
</script>

<?php
    include PATH.'parts/footer.php';