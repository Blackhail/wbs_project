<?php declare( strict_types = 1 );

$site_name = 'artist name'; /* hier später generierter artist name*/

require_once '../bootstrap.php';

$auth_id = auth_id();
$get_user_id = $_GET['id'];

/////////// CHANGE DB CONTENTS ///////////////////////////////

if (request_is('post')) {
    switch (request('action')) :
        
        case 'edit_maininfo':

            // $avatar = $_POST['avatar'];
            $name = $_POST['name'];
            $website = $_POST['website'];
            $specials = $_POST['specials'];
            $password = $_POST('password');

            $errors = validate($password , $password_confirmation);

            if ( ! $errors) {
            db_update('users', (int) $get_user_id , [
                // 'avatar' => 'images/avatars/default.png',
                'website' => $website,
                'specials' => $specials,
                'name' => $name,
                'password' => password_hash($password , PASSWORD_DEFAULT),              
            ]);
            }
            break;

            case 'edit_description':

                $description = $_POST['description'];

                db_update('users', (int) $get_user_id , [
                    'description' => $description,
                ]); 
            break;

    endswitch;
}

// $test = '3j)ß6';
// preg_match('^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$' , $test;
// print_r($test);

function validate($password , $password_confirmation) {
    $errors = [];

    // if (!preg_match('^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$' , $password)) {
    //     $errors['password'] = 'Please enter a password!';
    // }
    if ($password !== $password_confirmation) {
        $errors['password'] = 'The passwords do not match!';
    }
}

/////////// READ DB ////////////////////////////////////////

$sql_profile = "SELECT `id` ,  `name` , `description` , `avatar` , `website` , `specials` FROM `users`";
$profiles = db_raw_select($sql_profile);

$sql_media = "SELECT  `url` , `user_id` , `header_image` , `project_id` FROM `media`";
$medias = db_raw_select($sql_media);

$sql_project = "SELECT `name`  , `user_id` , `id` FROM `project`";
$projects = db_raw_select($sql_project);

/// SITENAME //////////////////////////////////////////////////////////////////////////

foreach ($profiles as $profile) {
    if ($get_user_id === $profile['id']) {
        $site_name = $profile['name'];
    }
}

/// HTML //////////////////////////////////////////////////////////////////////////////

include PATH.'parts/head.php'; ?>

<main>
    <div class="container-medium clear">
        <!-- PROFIL-INFO LEFT -->
        <div class="profile-info clear">
            <div class="profile-left">
                <?php foreach ($profiles as $profile) {
                    if ($get_user_id === $profile['id']) { ?>
                        <div class="profil-img">
                            <img src="<?= '../' . $profile['avatar'] ?>" alt="profile-img">
                        </div>
                        <div>
                            <h1><?= $profile['name'] ?></h1>
                            <h2><?= $profile['specials'] ?></h2>
                            <p><a href="<?= $profile['website'] ?>" target="blank">Website</a></p>
                        </div>
                        <!-- EDIT PROFIL INFO LEFT -->
                        <?php if ($get_user_id == auth_id()) : ?>
                            <div class="form-left"> 
                                <form action="profile_page.php?id=<?=$get_user_id?>" method="POST" enctype="multipart/form-data" >
                                    <!-- ////////////////////////////////////////////////////////////////// -->    
                                    <!-- profile info left email -->
                                    <p>change password</p>


                                    <label for="password">old passwort</label>
                                    <input type="text" name="old_password" id="password">
                                    <label for="password">newe passwort</label>
                                    <input type="text" name="password" id="password">
                                    <label for="password">confirm passwort</label>
                                    <input type="text" name="confirm_password" id="password">


                                    <!-- ////////////////////////////////////////////////////////////////// -->

                                    <!-- profile info left profile-img -->
                                    <label for="avatar">Change Profile Image</label>
                                    <input type="file" name="avatar" id="avatar">
                                    <!-- profile info left text -->
                                    <label for="name">Change User Name</label>
                                    <input type="text" name="name" value="<?= $profile['name'] ?>" id="name">
                                    <label for="specials">Change your specials</label>
                                    <input type="text" name="specials" value="<?= $profile['specials'] ?>" id="specials">
                                    <label for="website">Change your website</label>
                                    <input type="text" name="website" value="<?= $profile['website'] ?>" id="website">
                                    <a class="button" href="">cange your password</a>
                                    <!-- ende profil info links -->
                                    <button type="submit" name="action" value="edit_maininfo">save changes</button>
                                </form>
                            </div>
                        <?php endif; 
                     }
                } ?>                  
            </div>
            <!-- PROFIL INFO RIGHT -->
            <div class="profile-right">
                <?php foreach ($profiles as $profile) {
                    if ($get_user_id === $profile['id']) { ?>
                        <p class="describe"><?= $profile['description'] ?></p>
                        <!-- EDIT PROFIL INFO RIGHT -->
                        <?php if ($get_user_id == auth_id()) {
                            if ($profile['description'] === "") { ?>
                                <p class="describe">Bitte schreib etwas über dich!</p>
                            <?php } ?>
                            <form action="profile_page.php?id=<?=$get_user_id?>" method="POST">
                                <textarea name="description" id="description" cols="30" rows="10"><?= $profile['description'] ?></textarea>
                                <button type="submit" name="action" value="edit_description">save changes</button>
                            </form>
                        <?php }
                    }
                } ?>
            </div>
        </div>
        <!-- IMG-CONTAINER -->
        <div class="img-container">
            <div class="main-img">           
                <?php foreach ($medias as $media) {
                    if ($media['user_id'] === $get_user_id && $media['header_image']) { ?>
                        <figure>
                            <img src="<?= '../' . $media['url'] ?>">
                            <?php foreach ($projects as $project) {
                                if ($media['project_id'] === $project['id']) { ?>
                                    <figcaption><?=$project['name']?></figcaption>
                                <?php }
                            } ?>
                        </figure>
                        <?php if ($get_user_id  == auth_id()) : ?>
                            <a class="button" href="project_page.php?project_id=<?= $media['project_id']?>">edit project</a>
                            <button type="submit" name="action" value="delete_project">delete project</button>
                            <a class="button" href="new_project_page.php">new project</a>
                        <?php endif;
                    }
                }
                // create new project über extra Formular?
                // hier über extra Formular?
                if ($project['user_id']  === auth_id()) : ?>
                    <a class="button" href="new_project_page.php">new project</a>
                <?php endif; ?>    
            </div>
        </div>
    </div>
</main>


<?php
include PATH.'parts/footer.php';