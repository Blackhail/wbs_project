<?php

if (isset($_POST['submit'])) {

    $file = $_FILES['datei'];
    $fileName = $_FILES['datei']['name'];
    $fileSize = $_FILES['datei']['size'];
    $fileType = $_FILES['datei']['type'];
    $fileTmpName = $_FILES['datei']['tmp_name'];
    $fileError = $_FILES['datei']['error'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('jpg', 'jpeg', 'png', 'gif');

    if (in_array($fileActualExt, $allowed)){

        if ($fileError === 0) {

            if($fileSize < 500000) {

            $fileNameNew = uniqid('', true).".".$fileActualExt;

            $fileDesitination = 'upload/'.$fileNameNew;
            move_uploaded_file($fileTmpName, $fileDesitination);

            header("Location: index.php?uploadsuccess");

            } else {
                echo "Die Datei überschreitet die max. Größe";
            }

        } else {
            echo "Beim Hochladen ist ein Fehler aufgetreten";
        }

    } else {
        echo "Dieser Dateityp ist nicht erlaubt";
    }
}