<?php declare( strict_types = 1 );

$site_name = 'projects';

require_once '../bootstrap.php';




/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>

<main class="card-box">
    <div>
        <!-- Überschrift aus linktext in Navi holen -->
        <h1>Artists</h1>
        <ul>
            <li><a href="<?= url('profile_page.php') ?>" title="Animation">Animation</a></li>
            <li><a href="<?= url('profile_page.php') ?>"title="Design">Design</a></li>
            <li><a href="<?= url('profile_page.php') ?>" title="Interactiv">Interactive</a></li>
        </ul>
    </div>

    <div class="cards">

        <div class="card">
            <figure>
                <a href=""><img src="<?= url('../images/avatars/jan_koester_profile.jpg') ?>" alt=""></a>
                <figcaption>artist name / project name</figcaption>
            </figure>
        </div>

        <div class="card">
            <figure>
                <a href=""><img src="<?= url('../images/avatars/jan_koester_profile.jpg') ?>" alt=""></a>
                <figcaption>artist name / project name</figcaption>
            </figure>
        </div>

        <div class="card">
            <figure>
                <a href=""><img src="<?= url('../images/avatars/jan_koester_profile.jpg') ?>" alt=""></a>
                <figcaption>artist name / project name</figcaption>
            </figure>
        </div>

        <div class="card">
            <figure>
                <a href=""><img src="<?= url('../images/avatars/jan_koester_profile.jpg') ?>" alt=""></a>
                <figcaption>artist name / project name</figcaption>
            </figure>
        </div>

        <div class="card">
            <figure>
                <a href=""><img src="<?= url('../images/avatars/jan_koester_profile.jpg') ?>" alt=""></a>
                <figcaption>artist name / project name</figcaption>
            </figure>
        </div>

    </div>
</main>


<?php
    include PATH.'parts/footer.php';


// <!-------------------------- TEST READ -------------------------------->

foreach ($profile_cards as $profile_card) : 
    // var_dump ( $profile_card ) ;
    ?>
    
<div class="cards-test">
    <div class="card-test">
        <figure>

            <!-- if "Artists" clicked (if a-title = user-category)-->
            <a href="#"><img src="<?= '../' . $profile_card['avatar'] ?>" alt="profile-img"></a>
            <figcaption><?= $profile_card['name'] ?></figcaption>



            <!-- if "Artists" clicked (if a-title = user-category)-->
            <!-- <a href=""><img src="<?= $profile_card['avatar'] ?>" alt="profile-img"></a>
            <figcaption><?= $profile_card['name'] ?></figcaption> -->
        </figure>
    </div>
</div>

<?php endforeach; ?>

<!-- TEST READ ENDE -->