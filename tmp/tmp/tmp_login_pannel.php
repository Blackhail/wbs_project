<?php declare( strict_types = 1 );

require_once '../bootstrap.php';

$sql_profile = "SELECT * FROM `users`";
$profiles = db_raw_select($sql_profile);

?>


<div class="login-pannel">

    <a href="<?= url('index.php') ?>">Home</a>

    <?php if (!auth_id()) : ?>
        <a href="<?= url('auth/login.php') ?>">Login</a>
    <?php endif; ?>

    <?php if (auth_id()) : ?>
        <a href="<?= url('auth/logout.php') ?>">Logout</a>
    <?php endif; ?>
    
    <!-- todo - auf admin beschränken! -->
    <?php 
    
    
        foreach ($profiles as $profile) {
            if (auth_id() && $profile['admin']) { ?>

            
                <a href="<?= url('auth/register.php') ?>">Register</a>
            
        <?php }
    }
        
    ?>

</div>