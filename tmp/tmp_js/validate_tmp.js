;(function(window,document,$){
    'use strict';

  var settings = {
    // debug: true,
   
    errorElement: 'label',
    errorClass: 'error_required',
    validClass: 'valid',
 
    normalizer: function (value) {
        return $.trim(value);
    },

    errorPlacement: function ($errorElement, $element) {

      console.log($element, $errorElement);
        if ($element.prop('type') === 'radio') {
            $element.parent().parent().append($errorElement);
        } 
        else if ($element.prop('type') === 'checkbox') {
            $element.parent().append($errorElement);
        } 
        else {
            $element.after($errorElement); 
        }
        },

    rules: {
        vorname: {       
            pattern: /^[a-zäöüß\-.,()'"\s]+$/i,
        },
        nachname: {
            pattern: /^[a-zäöüß\-.,()'"\s]+$/i,
        },
        fax: {       
            pattern: /^[0-9()+\-\s]+$/i,
        },
        code: {
            pattern: /^[0-9\s]+$/i,
        },
        phone: {
            pattern: /^[+]*[(]{0,1}[0-9]{1,6}[)]{0,1}[-\s\./0-9]*$/g,
        },
        eaddress: {
            email: true
        }

    },

    messages: {
        nachname: {
            letterswithbasicpunc: 'Bitte nur Buchstaben und Interpunktion AUS messages'
        }
    }
  };

    
    $(function(){
        $('form').eq(0).validate(settings);
    });
  })(window,document,jQuery);