; (function (window, document, $) {
    'use strict';


    function rollUp(data) {
        var clicked = null;

        function handleClick(e) {
            e.preventDefault();
            $('#event').find('a').removeClass('current');
            $(this).addClass('current');
            clicked = this;

            $.ajax({
                url: '../lib/data/exercise_01.json',
                dataType: 'json',

                success: function (data) {
                    var len = Object.keys(data).length;
                    var key = clicked.id;
                    key.toUpperCase()

                    for (var i = 0; i < len; i++) {

                        if (key.toUpperCase() === Object.keys(data)[i]) {
                            var output = Object.entries(data)[i];
                            createOutput(output);
                        }
                    }
                }
            })
        }

        function createOutput(sessions) {
            $('#sessions').empty();
            $('#details').empty();
            $('#sessions').find('p').remove();
            $('#sessions').append('<ul/>');

            var len = sessions[1].length;
            for (var i = 0; i < len; i++) {
                var session = sessions[1];
                var sessArray = Object.entries(session)[i];
                var time = sessArray[1].time;
                var title = sessArray[1].title;
                $('#sessions').find('ul').append('<li><span class="time">' + time + '</span><a href="descriptions.html#' + title.split(' ').join('-') + '">' + title + '</a></li>');
            }

            $('#sessions').on('click', 'a', handleClickEvent);

        }

        function handleClickEvent(e) {
            e.preventDefault();
            $('#sessions').find('a').removeClass('current');
            $(this).addClass('current');
            var div = 'div#' + this.innerText.split(' ').join('-')
            var html = '';

            $.ajax({
                url: 'descriptions.html',
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                dataType: 'html',
                error: function () {
                    alert('Es ist ein Fehler aufgetreten! Bitte schmeißen Sie Ihren Rechner aus dem Fenster!')
                },
                success: function (data) {
                    html = $(data).filter(div);
                    $('#details').empty().append(html);
                }
            });
        }

        $('#event').on('click', 'a', handleClick);
    }

    $(function () {
        $.ajax({
            url: '../lib/data/exercise_01.json',
            dataType: 'json',
            success: function (data) {
                rollUp(data);
            }
        })
    });

})(window, document, jQuery);