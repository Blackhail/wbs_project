<?php declare(strict_types=1);

const PATH = 'C:/xampp/htdocs/projects_on_git/grimm/';

const APP_DOMAIN = 'localhost';
const APP_BASE_URL = '/projects_on_git/grimm/public';

foreach ([
    'helpers',
    'request',
    'session',
    'authentication',
    'database_classic',
    'view'
] as $module) {
    require_once PATH."lib/$module.php";
}

$database = db_connect([
    'host' => 'localhost',
    'username' => 'root',
    'password' => '',
    'database' => 'grimm'
]);

session_start();

$errors = [];
