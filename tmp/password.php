<?php declare(strict_types=1);

require_once '../../bootstrap.php';

$user_id = (int) auth_id();

// if ( !$errors && !password_verify($password , $user['password'])) {
//     $errors['email'] = 'Failed! Please check your input!';
// }


$user = db_raw_first('SELECT * FROM `users` WHERE `id` = ' . db_prepare($user_id));


if (request_is('post')) {
    
    $password_old = request('password_old');
    $password = request('password');
    $password_confirmation = request('password_confirmation');
    
    var_dump("pw_alt", $password_old);
    var_dump("pw", $password);
    var_dump("pw_conf", $password_confirmation);
    var_dump("user", $user);
    sleep(1);
    
    $errors = validate($password , $password_old, $password_confirmation);
    
    // if ( ! $errors) {
    //     $user = db_raw_first('SELECT * FROM `users` WHERE `id` = $user_id');
    // }

    var_dump("fehler", $errors);
    
    if ( ! $errors) {
        db_update('users' , $user_id, [
            'password' => password_hash($password_confirmation, PASSWORD_DEFAULT),
            ] );
        }
        //logout();
        // redirect('auth/login.php');
    }
    
function validate($password , $password_old, $password_confirmation) {
    $errors = [];
    $user_id = (int) auth_id();
    $user = db_raw_first('SELECT * FROM `users` WHERE `id` = ' . db_prepare($user_id));
    
    if ($password === '') {
        $errors['password'] = 'Bitte geben Sie das gewünschte Passwort ein!';
    }
    
    if (!password_verify($password_old , $user['password'])) {
        $errors['password_old'] = 'Ihr altes Passwort stimmt nicht';
    }

    if ($password !== $password_confirmation) {
        $errors['password_confirmation'] = 'Sie haben unterschiedliche Passwörter eingegeben!';
    }


    
    return $errors;

}

/*************************** HTML ****************************/

include PATH.'parts/header.php'; ?>

<div class="wrapper">
    <div class="form-container">
        <div>

            <form action="" method="POST" enctype="multipart/form-data" >
                <h3>Change your password:</h3>
                <label class="require" for="password_old">Altes Password</label>
                <input type="text" name="password_old" id="password_old" require="require">
                
                <label class="require" for="password">Neues Passwort</label>
                <input class="require" type="text" name="password" id="password" require="require">
                
                <label for="password_confirmation">Passwort bestätigen</label>
                <input class="require" type="text" name="password_confirmation" id="password_confirmation" require="require">
                
                <button type="submit" name="action" value="change_password">Änderung speichern</button>
            </form>
        </div>
        
    </div>
    
</div>