<?php declare( strict_types = 1 );

$site_name = 'artist name'; /* hier später generierter artist name*/

require_once '../bootstrap.php';

$auth_id = auth_id();
$get_user_id = $_GET['id'];

/////////// CHANGE DB CONTENTS //////////////////////////////////////////////////////

if (request_is('post')) {
    switch (request('action')) :
        
        case 'edit_maininfo':

            $name = $_POST['name'];
            $website = $_POST['website'];
            $specials = $_POST['specials'];
            $file = $_FILES['avatar'];
            $fileName = 'images/avatars/' . $_FILES['avatar']['name'];
            $fileSize = $_FILES['avatar']['size'];
            $fileType = $_FILES['avatar']['type'];
            $fileTmpName = $_FILES['avatar']['tmp_name'];
            $fileError = $_FILES['avatar']['error'];
            // 'images/avatars/' . 

            // security für file upload einbauen!!!!!!
        
            $fileExt = explode('.', $fileName);
            $fileActualExt = strtolower(end($fileExt));
            $allowed = array('jpg', 'jpeg', 'png', 'gif');
        
            if (in_array($fileActualExt, $allowed)){
        
                if ($fileError === 0) {
        
                    if($fileSize < 500000) {
        
                    $fileNameNew = uniqid('', true).".".$fileActualExt;
                    $fileDesitination = '../../images/avatars/'.$fileNameNew;
                    move_uploaded_file($fileTmpName, $fileDesitination);
                    header("Location: index.php?uploadsuccess");
                    } else {
                        echo "The file exceeds the maximum size!";
                    }
                } else {
                    echo "An error occurred during upload!";
                }
            } else {
                echo "This file type is not allowed!";
            }

            // end security für file upload

            if ( ! $errors) {
            db_update('users', (int) $get_user_id , [
                'website' => $website,
                'specials' => $specials,
                'name' => $name,
                'avatar' => $fileName /* zu db insert?? */
            ]);

            // hier db insert für img???

            }
            break;

            case 'edit_description':

                $description = $_POST['description'];

                db_update('users', (int) $get_user_id , [
                    'description' => $description,
                ]); 
            break;

    endswitch;
}

/////////// READ DB //////////////////////////////////////////////////////////////////

$sql_profile = "SELECT `id` ,  `name` , `description` , `avatar` , `website` , `specials` FROM `users`";
$profiles = db_raw_select($sql_profile);

$sql_media = "SELECT  `url` , `user_id` , `header_image` , `project_id` FROM `media`";
$medias = db_raw_select($sql_media);

$sql_project = "SELECT `name`  , `user_id` , `id` FROM `project`";
$projects = db_raw_select($sql_project);

/// SITENAME //////////////////////////////////////////////////////////////////////////

foreach ($profiles as $profile) {
    if ($get_user_id === $profile['id']) {
        $site_name = $profile['name'];
    }
}

/// HTML //////////////////////////////////////////////////////////////////////////////

include PATH.'parts/head.php'; ?>

<main>
    <div class="container-medium clear">
        <div class="profile-info clear">
            <!-- PROFIL-INFO LEFT -->
            <div class="profile-left">
                <?php foreach ($profiles as $profile) {
                    if ($get_user_id === $profile['id']) { ?>
                        <div class="profil-img">
                            <img src="<?= '../' . $profile['avatar'] ?>" alt="profile-img">
                        </div>
                        <div class="short-info">
                            <h1><?= $profile['name'] ?></h1>
                            <h2><?= $profile['specials'] ?></h2>
                            <p><a href="<?= $profile['website'] ?>" target="blank">Website</a></p>
                        </div>
                        <?php                     
                     }
                } ?>                  
            </div>
            <!-- PROFIL INFO RIGHT -->
            <div class="profile-right">
                <?php foreach ($profiles as $profile) {
                    if ($get_user_id === $profile['id']) { ?>
                        <p class="describe"><?= $profile['description'] ?></p>
                        <!-- IF DESCRIPTION EMPTY -->
                        <?php if ($get_user_id == auth_id()) {
                            if ($profile['description'] === "") { ?>
                                <p class="describe">Bitte schreib etwas über dich!</p>
                            <?php 
                            }
                        }
                    }
                }
                ?>
            </div>
        </div>
        <!-- FORM-CONTAINER -->
        <div class="form clear">
            <h2>Here you can edit your profile</h2>
            <?php if ($get_user_id == auth_id()) : ?>
                <div class="form-left">
                    <form action="profile_page.php?id=<?=$get_user_id?>" method="POST" enctype="multipart/form-data" >
                    <!-- profile info left change password -->
                        <p>change password</p>
                        <label class="require" for="password">old passwort</label>
                        <input type="text" name="old_password" id="password" require="require">
                        <label class="require" for="password">new passwort</label>
                        <input class="require" type="text" name="password" id="password" require="require">
                        <label for="password">confirm passwort</label>
                        <input class="require" type="text" name="confirm_password" id="password" require="require">
                        <button type="submit" name="action" value="change_password">save changes</button>
                    </form>
                    <form action="profile_page.php?id=<?=$get_user_id?>" method="POST" enctype="multipart/form-data" >
                        <!-- profile info left profile-img -->
                        <label for="avatar">Change Profile Image</label>
                        <input type="file" name="avatar" id="avatar">
                        <!-- profile info left text -->
                        <label for="name">Change User Name</label>
                        <input type="text" name="name" value="<?= $profile['name'] ?>" id="name">
                        <label for="specials">Change your specials</label>
                        <input type="text" name="specials" value="<?= $profile['specials'] ?>" id="specials">
                        <label for="website">Change your website</label>
                        <input type="text" name="website" value="<?= $profile['website'] ?>" id="website">
                        <!-- ende profil info links -->
                        <button type="submit" name="action" value="edit_maininfo">save changes</button>
                    </form>
                </div>
            <?php endif;
            ?>
            <?php if ($get_user_id == auth_id()) : ?>
            <div class="form-right">
                <form action="profile_page.php?id=<?=$get_user_id?>" method="POST">
                                <p>Here you can change your short description.</p>
                                <textarea name="description" id="description" cols="70" rows="10" placeholder="Here you can change your short description."><?=$profile['description'] ?></textarea>
                                <button type="submit" name="action" value="edit_description">save changes</button>
                            </form>
                        <a class="button" href="new_project_page.php">new project</a>
                </form>
            </div>
             <?php endif; ?>
        </div>
        <!-- END FORM-CONTAINER -->

        <!-- IMG-CONTAINER -->
        <div class="img-container">
            <div class="main-img">
                <?php foreach ($medias as $media) {
                    if ($media['user_id'] === $get_user_id && $media['header_image']) { ?>
                        <figure>
                            <img src="<?= '../' . $media['url'] ?>">
                            <?php foreach ($projects as $project) {
                                if ($media['project_id'] === $project['id']) { ?>
                                    <figcaption><?=$project['name']?></figcaption>
                                <?php }
                            } ?>
                        </figure>
                        <?php if ($get_user_id  == auth_id()) : ?>
                            <a class="button" href="project_page.php?project_id=<?= $media['project_id']?>">edit project</a>
                            <button type="submit" name="action" value="delete_project">delete project</button>
                            <!-- <a class="button" href="new_project_page.php">new project</a> -->
                        <?php endif;
                    }
                } ?>
            </div>
        </div>
    </div>
</main>


<?php
include PATH.'parts/footer.php';