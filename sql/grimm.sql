-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 29. Okt 2020 um 13:03
-- Server-Version: 10.4.13-MariaDB
-- PHP-Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `grimm`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_image` tinyint(1) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `media`
--

INSERT INTO `media` (`id`, `project_id`, `title`, `url`, `header_image`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'our man in nirvana main img', 'images/user/jan/our_man_In_nirvana/hero_visual_ourman_main_img.jpg', 1, 3, '2020-10-23 15:35:48', '2020-10-27 09:08:49'),
(2, 1, 'our man in nirvana', 'images/user/jan/our_man_in_nirvana/visual1_ourmaninnirvana.jpg', NULL, 3, '2020-10-23 15:39:59', '2020-10-23 16:49:41'),
(3, 1, 'our man in nirvana', 'images/user/jan/our_man_in_nirvana/visual3_ourmaninnirvana.jpg', NULL, 3, '2020-10-23 15:59:46', '2020-10-23 16:49:53'),
(4, 1, 'our man in nirvana', 'images/user/jan/our_man_in_nirvana/visual4_ourmaninnirvana.jpg', NULL, 3, '2020-10-23 16:00:42', '2020-10-23 16:50:04'),
(5, 1, 'our man in nirvana', 'images/user/jan/our_man_in_nirvana/visual5_ourmaninnirvana.jpg', NULL, 3, '2020-10-23 16:03:44', '2020-10-23 16:50:17'),
(6, 1, 'our man in nirvana', 'images/user/jan/our_man_in_nirvana/visual6_ourmaninnirvana.jpg', NULL, 3, '2020-10-23 16:04:29', '2020-10-27 09:09:07'),
(7, 1, 'our man in nirvana', 'images/user/jan/our_man_in_nirvana/visual7_ourmaninnirvana.jpg', NULL, 3, '2020-10-23 16:06:33', '2020-10-23 16:50:37'),
(8, 1, 'our man in nirvana', 'images/user/jan/our_man_in_nirvana/visual8_ourmaninnirvana.jpg', NULL, 3, '2020-10-23 16:07:04', '2020-10-27 09:07:27'),
(9, 2, 'möwen', 'images/user/gregor/moewen/still_00.jpg', 1, 4, '2020-10-23 16:17:10', '2020-10-23 16:50:50'),
(10, 2, 'möwen', 'images/user/gregor/moewen/still_01.jpg', NULL, 4, '2020-10-23 16:20:00', '2020-10-23 16:50:52'),
(11, 2, 'möwen', 'images/user/gregor/moewen/still_02.jpg', NULL, 4, '2020-10-23 16:20:38', '2020-10-23 16:50:54'),
(12, 2, 'möwen', 'images/user/gregor/moewen/still_03.jpg', NULL, 4, '2020-10-23 16:21:23', '2020-10-23 16:50:56'),
(13, 2, 'möwen', 'images/user/gregor/moewen/still_04.jpg', NULL, 4, '2020-10-23 16:21:46', '2020-10-23 16:50:58'),
(14, 2, 'möwen', 'images/user/gregor/moewen/still_05.jpg', NULL, 4, '2020-10-23 16:22:08', '2020-10-23 16:51:01'),
(15, 3, 'eine villa mit pinien', 'images/user/jan/eine_villa_mit_pinien/hero_visual_pinien_main_img.jpg', 1, 3, '2020-10-23 16:28:45', '2020-10-27 09:07:48'),
(16, 3, 'eine villa mit pinien', 'images/user/jan/eine_villa_mit_pinien/visual1_villapinien.jpg', NULL, 3, '2020-10-23 16:34:12', '2020-10-23 16:51:15'),
(17, 3, 'eine villa mit pinien', 'images/user/jan/eine_villa_mit_pinien/visual2_villapinien.jpg', NULL, 3, '2020-10-23 16:34:40', '2020-10-23 16:51:21'),
(18, 3, 'eine villa mit pinien', 'images/user/jan/eine_villa_mit_pinien/visual3_villapinien.jpg', NULL, 3, '2020-10-23 16:35:23', '2020-10-23 16:51:27'),
(19, 3, 'eine villa mit pinien', 'images/user/jan/eine_villa_mit_pinien/visual4_villapinien.jpg', NULL, 3, '2020-10-23 16:35:53', '2020-10-23 16:51:33'),
(20, 3, 'eine villa mit pinien', 'images/user/jan/eine_villa_mit_pinien/visual5_villapinien.jpg', NULL, 3, '2020-10-23 16:37:36', '2020-10-23 16:51:37'),
(21, 3, 'eine villa mit pinien', 'images/user/jan/eine_villa_mit_pinien/visual6_villapinien.jpg', NULL, 3, '2020-10-23 16:37:38', '2020-10-23 16:51:41'),
(22, 3, 'eine villa mit pinien', 'images/user/jan/eine_villa_mit_pinien/visual7_villapinien.jpg', NULL, 3, '2020-10-23 16:38:43', '2020-10-27 09:10:54'),
(23, 5, 'value of soil', 'images/user/pauline/value_of_soil/hero_visual_the_value_of_soil__mainimg.jpg', 1, 7, '2020-10-25 14:01:56', '2020-10-25 14:11:20'),
(24, 5, 'value of soil', 'images/user/pauline/value_of_soil/visual1_value_of_soil.jpg', NULL, 7, '2020-10-25 14:03:14', '2020-10-25 14:10:53'),
(25, 5, 'value of soil', 'images/user/pauline/value_of_soil/visual2_value_of_soil.jpg', NULL, 7, '2020-10-25 14:03:49', '2020-10-25 14:11:08'),
(26, 5, 'value of soil', 'images/user/pauline/value_of_soil/visual2_value_of_soil.jpg', NULL, 7, '2020-10-25 14:04:19', '2020-10-25 14:10:09'),
(27, 5, 'value of soil', 'images/user/pauline/value_of_soil/visual3_value_of_soil.jpg', NULL, 7, '2020-10-25 14:04:46', '2020-10-25 14:10:04'),
(28, 4, 'bango vassil', 'images/user/milen/bango_vassil/hero_visual_bango_vassil_main_img.jpg', 1, 8, '2020-10-25 14:07:12', '2020-10-25 14:09:22'),
(29, 4, 'bango vassil', 'images/user/milen/bango_vassil/visual1_bango_vassil.jpg', NULL, 8, '2020-10-25 14:12:59', '2020-10-25 14:13:27'),
(30, 4, 'bango vassil', 'images/user/milen/bango_vassil/visual2_bango_vassil.jpg', NULL, 8, '2020-10-25 14:13:29', '2020-10-25 14:13:56'),
(31, 4, 'bango vassil', 'images/user/milen/bango_vassil/visual3_bango_vassil.jpg', NULL, 8, '2020-10-25 14:14:01', '2020-10-25 14:14:28'),
(32, 4, 'bango vassil', 'images/user/milen/bango_vassil/visual4_bango_vassil.jpg', NULL, 8, '2020-10-25 14:14:51', '2020-10-25 14:15:15'),
(33, 4, 'bango vassil', 'images/user/milen/bango_vassil/visual5_bango_vassil.jpg', NULL, 8, '2020-10-25 14:15:31', '2020-10-25 14:15:54'),
(34, 4, 'bango vassil', 'images/user/milen/bango_vassil/visual6_bango_vassil.jpg', NULL, 8, '2020-10-25 14:16:05', '2020-10-25 14:16:35');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `project`
--

CREATE TABLE `project` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_category` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(511) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `project`
--

INSERT INTO `project` (`id`, `user_id`, `name`, `project_category`, `sub_category`, `description`, `created_at`, `updated_at`) VALUES
(1, 3, 'Our Man in Nirvana', 'Animation', '2D Animation', 'JAN: A famous rockstar dies accidently during a concert and awakes in nirvana. There he has to face his wishes and deeds.', '2020-10-23 15:32:27', '2020-10-28 19:48:22'),
(2, 4, 'Möwen', 'Design', 'Graphic Novels', 'GREGOR: Hallo. Ich bin ein kleiner Blindtext. Und zwar schon so lange ich denken kann. Es war nicht leicht zu verstehen, was es bedeutet, ein blinder Text zu sein: Man ergibt keinen Sinn. Wirklich keinen Sinn. Man wird zusammenhangslos eingeschoben und rumgedreht – und oftmals gar nicht erst gelesen. ', '2020-10-23 16:14:54', '2020-10-28 19:48:32'),
(3, 3, 'Eine Villa mit Pinien', 'Animation', '2D Animation', 'JAN: Lion and Bird break into an uninhabited villa to find out about the reason why it is not aging. ', '2020-10-23 16:27:25', '2020-10-28 19:48:43'),
(4, 8, 'Bango Vassil', 'Interactive', '2D Animation', 'MILEN: Two children share an altogether different kind of New Year\'s Eve, taking an icy journey where they must cross waters and cultures to make a fresh start.', '2020-10-25 13:52:35', '2020-10-28 19:49:14'),
(5, 7, 'Value of Soil', 'Design', 'Graphic novels', 'PAULINE: Film for the The Economics of Land Degradation Initiative (ELD Initiative) produced by Climate Media Factory.\r\nMore information about the project here: www.eld-initiative.org ', '2020-10-25 13:59:09', '2020-10-28 19:49:42');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `artist_category` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1023) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specials` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `artist_category`, `description`, `avatar`, `website`, `specials`, `created_at`, `updated_at`, `admin`) VALUES
(1, 'undine@grimm.de', '$2y$10$92IcsGHx18Y7HvOkP5M5dulmUr3tKnPyw6eGhpRdJfA6ej5s/vqeS', 'grimm', '', '', 'images/avatars/default.jpg', '', '', '2020-08-28 17:44:24', '2020-10-27 17:44:28', NULL),
(2, 'undine.aust@gmx.de', '$2y$10$5XfzNwXGHYaXWXtGrAVkleUQ.czaojdSyXQcmn8QYEf3TMu.jPuSm', 'admin', '', '', 'images/avatars/default.jpg', '', '', '2020-10-21 15:55:23', '2020-10-27 17:43:06', 1),
(3, 'jan@ourmaninnirvana.com', '$2y$10$HxF90rDsPcrg0SXsn7Fcvu4UbL3R8plczEmbUo7skS2jI9oEGqSn6', 'Jan', 'Animation', 'Beschreibung Profil Jan\r\n', 'images/avatars/jan_koester_profile.jpg', 'https://jankoester.com', '2D Animation, Graphic Design', '2020-10-22 11:55:44', '2020-10-29 12:06:32', 1),
(4, 'gregor.dashuber@gmail.com', '$2y$10$t3YTHrQslIAPHFfNL.rOZOKFhCoxYbnVmeFAbRff7KRq598AfqxUu', 'Gregor', 'Interactive', 'Gregor Dashuber graduated from the HFF Konrad Wolf in 2009 after winning the prestigious le. ', 'images/avatars/gregor_dashuber.png', '', '', '2020-10-22 11:56:37', '2020-10-29 08:30:56', NULL),
(5, 'sonja@talking-animals.com', '$2y$10$OJkfKEBksF0qWCEeanEE.OvYzQQHiP..cqb34HZrlu/3TC9nQ3kUy', 'Sonja', 'Design', 'Sonja graduated in animation at the HFF \"Konrad Wolf\" with her film COCOON CHILD, which could be seen at the Berlinale Generation Competition in 2009. Her next short film TANTO TANTO was commissioned by german television 3sat/ZDF in 2010. Since 2008 she directed and animated commercials, installations and music videos. In 2012 she started to give lectures at the HFF (University for Film and Television) and in 2014 she finished her new short film DAME MIT HUND.', 'images/avatars/sonja_rohleder.jpg', '', '', '2020-10-22 11:57:45', '2020-10-29 08:32:02', NULL),
(6, 'test1@grimm.de', '$2y$10$NpFQaHzoi1m6XjVsNQJTZOGJ67q3p6t1zdvA7iEH2QXQOReKb.7vW', 'testuser1', 'Animation', '', 'images/avatars/default.jpg', 'https://www.w3schools.com', '3d animation', '2020-10-23 11:26:37', '2020-10-27 17:43:27', NULL),
(7, 'contact@paulinekortmann.de', '$2y$10$lYYVg6lAXg2FCKTr4q2mue5qZm/YXft2LRzBQVnXTmnKm3GNDGkjy', 'Pauline', 'Animation', 'Pauline was born in Berlin. After an apprenticeship as digital media designer at a children\'s computer game company she studied Animation at Film and Television Academy (HFF) „Konrad Wolf“ in Potsdam-Babelsberg from 2004-2010. Since then she works free-lance as animator, director and illustrator. ', 'images/avatars/pauline_kortmann_profile.jpg', 'https://www.paulinekortmann.de', '2D drawn animation', '2020-10-25 13:27:27', '2020-10-27 17:43:34', NULL),
(8, 'milen@my-happy-end.com', '$2y$10$DFAg2QJBm9AJb0jiPFHwsePOiSskZ9xgibLxZyNOEZNxA7YSKFgPG', 'Milen', 'Animation', 'Milen Vitanov was born in Prague and raised in Sofia, where he spent many sleepless nights drawing on his first self-made light box. He graduated Animation in 2007 at the film academy Konrad Wolf in Babelsberg and committed himself to the alchemy of 2D/3D-Animation-Mixtures. His animation shorts have participated has participated in many international festival and received numerous prestigious awards. Milen co-founded Talking Animals in 2009 and is now based in Berlin, spending sleepless nights again while working on his new 2D/3D mixture-film.', 'images/avatars/milen_vitanow_profile.jpg', 'http://milen-vitanov.com/', 'Director, 2D Animation', '2020-10-25 13:52:02', '2020-10-26 09:06:15', NULL),
(9, 'christian@juhu.de', '$2y$10$zythbl20uAMiQpIzSLP7m.zCxUqhZfCXirBEvl7XTFyVKJnZeMUQC', 'christian', 'Animation', '', 'images/avatars/default.jpg', 'https://www.php-kurs.com/array-inhalt-ausgeben.htm', '2d animation', '2020-10-26 18:23:37', '2020-10-27 17:43:41', NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_user` (`user_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indizes für die Tabelle `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_user` (`user_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT für Tabelle `project`
--
ALTER TABLE `project`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `media_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints der Tabelle `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
