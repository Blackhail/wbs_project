;
(function (window, document, $) {
    'use strict';

    $.validator.addMethod("letterswithbasicpunc", function (value, element) {
        return this.optional(element) || /^[a-zäöüß\-.,()'"\s]+$/i.test(value);
    }, 'Bitte nur Buchstaben und Interpunktion');

    var settings = {
        debug: true,

        submitHandler: function () {
            alert('Alles koreekt! Ihre Daten werden nun an den Verfassungsschutz übertragen!');
        },
        errorElement: 'div',
        errorClass: 'error',
        normalizer: function (value) {
            return $.trim(value);
        },
        errorPlacement: function ($errorElement, $element) {
            $element.after($errorElement);
        },

        rules: {
            name: {
                letterswithbasicpunc: true
            },
            email: {
                email: true
            },
            password: {
                minlength: 8
            },
            'conf-password': {
                equalTo: '#password'
            }
        },

        messages: {
            name: {
                name: 'Field is required',
                letterswithbasicpunc: 'Bitte nur Buchstaben!'
            },
        },
    }

    $(function () {
        $('form').find('input').eq(0).attr('title', 'This field is required.');
        $('form').eq(0).validate(settings);
    });

})(window, document, jQuery);