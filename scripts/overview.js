; (function (window, document, $) {
    'use strict';
    console.log('TEST');

    function showCards(data) {
        var data = data;
        console.log(data);
        console.log(data.length);

        function handleSubClick(e) {
            e.preventDefault();

            var cards = $('.cards').eq(0).empty();

            for (var i = 0; i < data.length; i++) {
                
            var category = data[i]['artist_category'];
            var img = '<img src="' + data[i]['avatar'] + '" alt="profile-img">';
            var name = '<figcaption>' + data[i]['name'] + '</figcaption>';
            var card = '<div class="card"><a href="profile_page.php?id=' + data[i]['id'] + '"><figure>' + img + name + '</figure></a></div>';

                if (category === this.title) {
                    $(cards).append(card);
                }
            }
        }

        $('#subcat').find('a').on('click', handleSubClick);
    } 

    $(function () {
        $.ajax({
            type: 'GET',
            url: '../json/all_data.php',
            success: function (data) {
                showCards(data);
            }
        })
    });

})(window, document, jQuery);