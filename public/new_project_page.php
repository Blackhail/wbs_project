<?php declare( strict_types = 1 );

$site_name = 'artist name';

require_once '../bootstrap.php';

$get_user_id = auth_id();

/////////// CHANGE DB CONTENTS ///////////////////////////////
$errors = [];
if (request_is('post')) {
  
    $name = $_POST['name'];
    $description = $_POST['description'];
    $projectCategory = $_POST['project_category'];

    db_insert('project' , [
        'name' => e($name),
        'description' => e($description),
        'project_category' => e($projectCategory),
        'user_id' => (int) $get_user_id
        ]);

    // FILE UPLOAD
    $file = $_FILES['url'];
    $fileName = $_FILES['url']['name'];
    $fileSize = $_FILES['url']['size'];
    $fileType = $_FILES['url']['type'];
    $fileTmpName = $_FILES['url']['tmp_name'];
    $fileError = $_FILES['url']['error'];
    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed = array('jpg', 'jpeg', 'png', 'gif');

    if (in_array($fileActualExt, $allowed)){

        if ($fileError === 0) {

            if($fileSize < 5000000) {
                // ABFRAGE NEUE PROJECT ID
                $sql = "SELECT `id` FROM `project` ORDER BY `id` DESC LIMIT 1";
                $new_project_id = db_raw_select($sql);

                $fileNameNew =  $new_project_id[0]['id'] . '_header_img'.'.'.$fileActualExt;
                $fileDesitination = '../images/'.$fileNameNew;
                $url = $fileDesitination;
                var_dump($fileDesitination);
                move_uploaded_file($fileTmpName, $fileDesitination);
                // header("Location: new_project_page.php?uploadsuccess");
            } else {
                echo "The file exceeds the maximum size!";
            }
        } else {
            echo "An error occurred during upload!";
        }
    } else {
        echo "This file type is not allowed!";
    }
    // END FILE UPLOAD

    db_insert('media', [
        'url' => '../images/' . $fileNameNew,
        'header_image' => 1,
        'user_id' => $get_user_id,
        'project_id' => $new_project_id[0]['id']
        ]);        
}

/// SITENAME //////////////////////////////////////////////////////////////////////////

$site_name = 'new project';

/// HTML //////////////////////////////////////////////////////////////////////////////

include PATH.'parts/head.php'; ?>
<!-- <script src="<?= url('../scripts/validates.js') ?>"></script> -->

<main id="new-project">
    <div class="container-medium">
     <div class="new-project">
        <form action="" method="POST" enctype="multipart/form-data" >
            <!-- PROJECT-NAME -->
            <div>
                <label class="require">PROJECT NAME</label>
                <input type="text" name="name" placeholder="projekt name" required="required">
            </div>
            <!-- CATEGORY -->
            <div>
                <label class="require">PROJECT CATEGORY</label>
                <div>
                <input type="radio" name="project_category" value="Animation"> <label>Animation</label>
                </div>
                <div>
                <input type="radio" name="project_category" value="Design"> <label>Design</label>
                </div>
                <div>
                <input type="radio" name="project_category" value="Interactive"> <label>Interactive</label>
                </div>
            </div>
            <!-- PROJECT-DESCRIPTION -->
            <div class="textarea">
                <label class="require">PROJECT DESCRIPTION</label>
                <small>Maximum 1000 characters allowed.</small>
                <textarea name="description" cols="70" rows="10" placeholder="description" required="required"></textarea>
            </div>
            <!-- HEADER-IMG -->
            <div class="file">
                <label class="require" for="url">PROJECT HEADER IMAGE</label>
                <small>Image must be 1920 x 720 pixels. <br>Image resolution must be 96ppi.</small>
                <input type="file" name="url" id="url">
            </div>
            <!-- REQUIRE -->
            <small>Fields marked with * are required.</small>
            <!-- SUBMIT -->
            <div>
            <button type="submit" name="action" value="create">create new project</button>
            </div>
            </form>
        </div>
    </div>                     
</main>


<?php
include PATH.'parts/footer.php';