<?php declare( strict_types = 1 );

$site_name = 'About';

require_once '../bootstrap.php';




/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>

<main class="about">
        
    <div class="container-medium">
        <div class="main-img">
           <img src="<?= url('../images/illustrations/collection_1448976_1920.jpg') ?>">
        </div>
        <div>
           <p>
           We are a collective of animation filmmakers, graphic designers and game designers, who have been united under the name Grimm Berlin since 2018. We produce graphic designs, short films, music videos, animated documentaries, animations for advertising, explanatory films, game and VR designs. Depending on your needs, we can take over the entire realisation of projects from concept/script to final product or take care of certain aspects of production. Our team is well-rehearsed and, in addition, we have access to a large network of external partners which we can fall back on if necessary.
          </p>
        </div>
    </div>

    <div class="location">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2429.1744960193246!2d13.413425915832981!3d52.49408084590204!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a84fcc12566c69%3A0x8ce7afacdb3c31c9!2sGrimmstra%C3%9Fe%2027%2C%2010967%20Berlin!5e0!3m2!1sde!2sde!4v1603708715508!5m2!1sde!2sde" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>

    <div class="container-medium">
        <p>27 Grimmstraße 10967 Berlin | + 49 588 588 588 | <a href="mailto:info@studioname.com">info@studioname.com</a></p>
    </div>
    
</main>


<?php
    include PATH.'parts/footer.php';