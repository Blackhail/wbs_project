<?php declare( strict_types = 1 );

require_once '../bootstrap.php';

$auth_id = auth_id();
$get_user_id = $_GET['id'] ?? NULL;
$user_id = auth_id();

/////////// CHANGE DB CONTENTS //////////////////////////////////////////////////////

$errors = [];

if (request_is('post')) {

    switch (request('action')) :

        case 'change_password':
        
            $old_password = request('old_password');
            $password = request('password');
            $password_confirm = request('confirm_password');

            $errors = validate($password ,  $old_password, $password_confirm);

            if (!$errors)
            db_update('users', (int) $user_id, [
                'password' => password_hash($password, PASSWORD_DEFAULT),
            ]);
        break;

        case 'edit_maininfo':

            $name = $_POST['name'];
            $website = $_POST['website'];
            $specials = $_POST['specials'];

            // <!-- FILE UPLOAD HANDLING -->
            $file = $_FILES['avatar'];
            $fileName = $_FILES['avatar']['name'];
            $fileSize = $_FILES['avatar']['size'];
            $fileType = $_FILES['avatar']['type'];
            $fileTmpName = $_FILES['avatar']['tmp_name'];
            $fileError = $_FILES['avatar']['error'];
        
            $fileExt = explode('.', $fileName);
            $fileActualExt = strtolower(end($fileExt));
            $allowed = array('jpg', 'jpeg', 'png', 'gif');

            if (in_array($fileActualExt, $allowed)){
        
                if ($fileError === 0) {
                    if($fileSize < 500000) {
                        $fileNameNew = $auth_id . "_profile." . $fileActualExt;
                        $fileDesitination = '../images/avatars/'.$fileNameNew;
                        $avatar = $fileDesitination;
                        move_uploaded_file($fileTmpName, $fileDesitination);
                        header("Location: index.php?uploadsuccess");
                    } else {
                        $errors['avatar'] = "The file exceeds the maximum size!";
                    }
                } else {
                    $errors['avatar'] = "An error occurred during upload!";
                }
            } else {
                $errors['avatar'] = "This file type is not allowed! If you have not uploaded a new picture, you can ignore this message.";
            }

            if ( ! $errors) {
                    db_update('users', (int) $get_user_id , [
                        'avatar' => $avatar
                    ]);
                }

            db_update('users', (int) $get_user_id , [
                'website' => $website,
                'specials' => $specials,
                'name' => $name,
            ]);
            break;

        case 'edit_description':

            $description = trim(e($_POST['description']));

            db_update('users', (int) $get_user_id , [
                'description' => $description,
            ]); 
        break;
    endswitch;
}

function validate($password ,  $old_password, $password_confirm) {
    $errors = [];
    $user_id = (int) auth_id();
    $user = db_raw_first('SELECT * FROM `users` WHERE `id` = ' . db_prepare($user_id));
    
    if ($password === '') {
        $errors['password'] = 'Password is empty!';
    }
    
    if (!password_verify($old_password , $user['password'])) {
        $errors['old_password'] = 'No Match with old password!';
    }

    if ($password !== $password_confirm) {
        $errors['confirm_password'] = 'No Match with new password!';
    }
    return $errors;
}

/////////// READ DB //////////////////////////////////////////////////////////////////

$sql_profile = "SELECT `id` ,  `name` , `description` , `avatar` , `website` , `specials` FROM `users`";
$profiles = db_raw_select($sql_profile);

$sql_media = "SELECT  `url` , `user_id` , `header_image` , `project_id` FROM `media`";
$medias = db_raw_select($sql_media);

$sql_project = "SELECT `name`  , `user_id` , `id` FROM `project`";
$projects = db_raw_select($sql_project);

/// SITENAME //////////////////////////////////////////////////////////////////////////

foreach ($profiles as $profile) {
    if ($get_user_id === $profile['id']) {
        $site_name = $profile['name'];
    }
}

/// HTML //////////////////////////////////////////////////////////////////////////////

include PATH.'parts/head.php'; ?>

<main id="profile-page">
    <!-- FALLBACK URL MANIPULATION -->
    <?php if (!$get_user_id) : ?>
        <h1 class="no-auth">You have no permission for this!</h1>
    <?php endif; ?>
    <div class="container-medium clear">
        <!-- PROFILE-INFO-CONTAINER -->
        <div class="profile-info clear">
            <!-- PROFIL-INFO LEFT -->
            <div class="profile-left">
                <?php foreach ($profiles as $profile) {
                    if ($get_user_id === $profile['id']) { ?>
                        <div class="profil-img">
                            <!-- FALLBACK PROFILE IMAGE -->
                            <?php if(!$profile['avatar'] === "") { ?>
                                <img src="<?= url('..images/avatars/default.jpg')?>" alt="fallback-img">
                            <?php }else {
                                ?>
                                <img src="<?= $profile['avatar'] ?>" alt="profile-img">
                            <?php }
                            ?>
                        </div>
                        <div class="short-info">
                            <h1><?= e($profile['name']) ?></h1>
                            <h2><?= e($profile['specials']) ?></h2>
                            <p><a href="<?= e($profile['website']) ?>" target="blank">Website</a></p>
                        </div>
                        <?php                     
                     }
                } ?>                  
            </div>
            <!-- PROFIL INFO RIGHT -->
            <div class="profile-right">
                <?php foreach ($profiles as $profile) {
                    if ($get_user_id === $profile['id']) { ?>
                        <p class="describe"><?= $profile['description'] ?></p>
                        <!-- IF DESCRIPTION EMPTY -->
                        <?php if ($get_user_id == auth_id()) {
                            if ($profile['description'] === "") { ?>
                                <p class="describe">Bitte schreib etwas über dich!</p>
                            <?php 
                            }
                        }
                    }
                }
                ?>
            </div>
        </div>
        <!-- FORM-CONTAINER -->
        <div class="form clear">
            <?php if ($get_user_id === auth_id()) : ?>
                <h2>Here you can edit your profile</h2>
            <?php foreach ($profiles as $profile) {
                if ($profile['id'] === auth_id()) : ?>
                    <div class="form-left clear">
                        <form action="profile_page.php?id=<?=$get_user_id?>" method="POST" enctype="multipart/form-data" >
                        <!-- profile info left - change password -->
                            <h3>Change your password:</h3>
                            <label class="require" for="password">old passwort</label>
                            <input type="text" name="old_password" id="password" require="require">
                            <label class="require" for="password">new passwort</label>
                            <input class="require" type="text" name="password" id="password" require="require">
                            <label for="password">confirm passwort</label>
                            <input class="require" type="text" name="confirm_password" id="password" require="require">
                            <button type="submit" name="action" value="change_password">save changes</button>
                        </form>
                        <form action="profile_page.php?id=<?=$get_user_id?>" method="POST" enctype="multipart/form-data" >
                            <!-- profile info left -  profile-img -->
                            <h3>Change your profile image:</h3>
                            <label for="avatar">Change Profile Image</label>
                            <?php if (isset($errors['avatar'])) : ?>
                                <div class="error">
                                    <?= $errors['avatar'] ?>
                                </div>
                            <?php endif; ?>
                            <input type="file" name="avatar" id="avatar">
                            <!-- profile info left  - text -->
                            <h3>Edit your main infos:</h3>
                            <label for="name">Change User Name</label>
                            <input type="text" name="name" value="<?= $profile['name']?>" id="name">
                            <label for="specials">Change your specials</label>
                            <input type="text" name="specials" value="<?= $profile['specials']?>" id="specials">
                            <label for="website">Change your website</label>
                            <input type="text" name="website" value="<?= $profile['website']?>" id="website">
                            <!-- ende edit profil infos -->
                            <button type="submit" name="action" value="edit_maininfo">save changes</button>
                        </form>
                    </div>
                    <div class="form-right">
                        <form action="profile_page.php?id=<?=$get_user_id?>" method="POST">
                                        <h3>Edit your artist description:</h3>
                                        <textarea name="description" id="description" cols="70" rows="10" placeholder="Here you can change your short description."><?=$profile['description'] ?></textarea>
                                        <button type="submit" name="action" value="edit_description">save changes</button>
                                    </form>
                                <a class="button" href="new_project_page.php?user_id=<?=$get_user_id?>">new project</a>
                        </form>
                    </div>
                <?php endif;
                }
            endif; ?>
        </div>
        <!-- IMG-CONTAINER -->
        <div class="img-container">
            <div class="main-img">
                <?php foreach ($medias as $media) {
                    if ($media['user_id'] === $get_user_id && $media['header_image']) { ?>
                        <figure>
                            <img src="<?= $media['url'] ?>">
                            <?php foreach ($projects as $project) {
                                if ($media['project_id'] === $project['id']) { ?>
                                    <figcaption><?=$project['name']?></figcaption>
                                <?php }
                            } ?>
                        </figure>
                        <?php if ($get_user_id  == auth_id()) : ?>
                            <a class="button" href="project_page.php?project_id=<?= $media['project_id']?>">edit project</a>
                        <?php endif;
                    }
                } ?>
            </div>
        </div>
    </div>
</main>
<?php
include PATH.'parts/footer.php';