<?php declare( strict_types = 1 );

$site_name = 'impressum'; /* hier später generierter artist name*/

require_once '../bootstrap.php';

/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>

<main>
    <div class="container-medium policy">
        <h1>Impressum</h1>
        <p>Informationspflicht laut § 5 TMG.</p>
        <ul>
            <li>Grimm Berlin</li>
            <li>Grimmsttraße 27</li>
            <li>12345 Berlin, </li>
            <li>Deutschland</li>
        </ul>
        <ul>
            <li>Tel.: 01234/56789</li>
            <li>Fax: 01234/56789-0</li>
            <li>E-Mail: jan@grimm.de</li>
        </ul>
        <p>
        Aufsichtsbehörde
        Bezirkshauptmannschaft Berlin
        Webseite der Aufsichtsbehörde
        https://www.aufsichtsbeoerde-berlin.de/
        Anschrift der Aufsichtsbehörde
        Behördengasse 1, 12345 Berlin
        </p>
        <p>Berufsbezeichnung: Animator, Grafiker</p>
        <h2>EU-Streitschlichtung</h2>
        <p>
        Gemäß Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-Verordnung) möchten wir Sie über die Online-Streitbeilegungsplattform (OS-Plattform) informieren.
        Verbraucher haben die Möglichkeit, Beschwerden an die Online Streitbeilegungsplattform der Europäischen Kommission unter http://ec.europa.eu/odr?tid=321227692 zu richten. Die dafür notwendigen Kontaktdaten finden Sie oberhalb in unserem Impressum.
        </p>
        <p>
        Wir möchten Sie jedoch darauf hinweisen, dass wir nicht bereit oder verpflichtet sind, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.
        </p>
        <h2>Haftung für Inhalte dieser Website</h2>
        <p>
        Wir entwickeln die Inhalte dieser Webseite ständig weiter und bemühen uns korrekte und aktuelle Informationen bereitzustellen. Laut Telemediengesetz (TMG) §7 (1) sind wir als Diensteanbieter für eigene Informationen, die wir zur Nutzung bereitstellen, nach den allgemeinen Gesetzen verantwortlich. Leider können wir keine Haftung für die Korrektheit aller Inhalte auf dieser Webseite übernehmen, speziell für jene die seitens Dritter bereitgestellt wurden. Als Diensteanbieter im Sinne der §§ 8 bis 10 sind wir nicht verpflichtet, die von ihnen übermittelten oder gespeicherten Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.
        </p>
        <p>
        Unsere Verpflichtungen zur Entfernung von Informationen oder zur Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen aufgrund von gerichtlichen oder behördlichen Anordnungen bleiben auch im Falle unserer Nichtverantwortlichkeit nach den §§ 8 bis 10 unberührt.
        </p>
        <p>
        Sollten Ihnen problematische oder rechtswidrige Inhalte auffallen, bitte wir Sie uns umgehend zu kontaktieren, damit wir die rechtswidrigen Inhalte entfernen können. Sie finden die Kontaktdaten im Impressum.
        </p>
        <h2>Haftung für Links auf dieser Website</h2>
        <p>Unsere Webseite enthält Links zu anderen Webseiten für deren Inhalt wir nicht verantwortlich sind. Haftung für verlinkte Websites besteht für uns nicht, da wir keine Kenntnis rechtswidriger Tätigkeiten hatten und haben, uns solche Rechtswidrigkeiten auch bisher nicht aufgefallen sind und wir Links sofort entfernen würden, wenn uns Rechtswidrigkeiten bekannt werden.</p>
        <p>Wenn Ihnen rechtswidrige Links auf unserer Website auffallen, bitte wir Sie uns zu kontaktieren. Sie finden die Kontaktdaten im Impressum.</p>
        <h2>Urheberrechtshinweis</h2>
        <p>Alle Inhalte dieser Webseite (Bilder, Fotos, Texte, Videos) unterliegen dem Urheberrecht der Bundesrepublik Deutschland. Bitte fragen Sie uns bevor Sie die Inhalte dieser Website verbreiten, vervielfältigen oder verwerten wie zum Beispiel auf anderen Websites erneut veröffentlichen. Falls notwendig, werden wir die unerlaubte Nutzung von Teilen der Inhalte unserer Seite rechtlich verfolgen.</p>
        <p>Sollten Sie auf dieser Webseite Inhalte finden, die das Urheberrecht verletzen, bitten wir Sie uns zu kontaktieren.</p>
        <h2>Bildernachweis</h2>
        <p>Die Bilder, Fotos und Grafiken auf dieser Webseite sind urheberrechtlich geschützt.</p>
        <p>Die Bilderrechte liegen bei den folgenden Fotografen und Unternehmen: Grimm Berlin</p>
    </div>
</main>

<?php
    include PATH.'parts/footer.php';