<?php declare(strict_types=1);
$site_name = 'Login';
require_once '../../bootstrap.php';

if ( request_is('post') ) {
    $email = request('email'); 
    $password = request('password');

    $errors = validate($email , $password);

    if ( ! $errors) {
        $user = db_raw_first( 'SELECT * FROM `users` WHERE `email` = ' . db_prepare($email) );
        if ( ! $user) {
            $errors['email'] = 'This user don\'t exist in our system';
        }
    }
    if ( !$errors && !password_verify($password , $user['password'])) {
        $errors['email'] = 'Failed! Please check your input!';
    }
    var_dump($errors);

    if (!$errors) {
        login($user);
        redirect('profile_page.php?id=' . auth_id());
    }
}

function validate($email , $password) {
    $errors = [];

    if ($email === '') {
        $errors['email'] = 'Please enter your email adress!';
    }
    if ($password === '') {
        $errors['password'] = 'Please enter a password!';
    }
    return $errors;
}

/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>

<div class="wrapper">
    <div class="form-container">
        <div>

            <form action="<?= url('auth/login.php') ?>" method="post">
                <h1>login</h1>
                <div class="input-field">
                    <label for="title">email:</label>
                    <?php if(isset($errors['email'])) : ?>
                        <div class="errors">
                            <?= $errors['email'] ?>
                        </div>
                    <?php endif; ?>
                    <input type="text" name="email" id="email" value="<?= $email ?? '' ?>">
                </div>
                <div class="input-field">
                    <label for="title">password:</label>
                    <?php if(isset($errors['password'])) : ?>
                        <div class="errors">
                            <?= $errors['password'] ?>
                        </div>
                    <?php endif; ?>
                    <input type="text" name="password" id="password">
                </div>
                <button type="submit">login</button>
            </form>

        </div>
        <p>Don't have an account? Go to <a href="<?= url('auth/register.php') ?>">registration page!</a></p>
    </div>
    
</div>

