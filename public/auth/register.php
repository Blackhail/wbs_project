<?php declare(strict_types=1);

require_once '../../bootstrap.php';

if (request_is('post')) {

    $name = request('name');
    $email = request('email');
    $password = request('password');
    $password_confirmation = request('password_confirmation');
    $artist_category = request('artist_category');

    $errors = validate($name , $email , $password , $password_confirmation , $artist_category);

    if ( ! $errors) {
        $user = db_raw_first('SELECT * FROM `users` WHERE `email` = ' . db_prepare($email) );
        if ($user) {
            $errors['email'] = 'This user already exist in our system.';
        }
    }
    if ( ! $errors) {
        db_insert('users' , [
            'name' => $name,
            'email' => $email,
            'password' => password_hash($password , PASSWORD_DEFAULT),
            'artist_category' => $artist_category
        ] );
        redirect('auth/login.php');
    }
}

function validate($name , $email , $password , $password_confirmation , $artist_category) {
    $errors = [];

    if ($name === '') {
        $errors['name'] = 'Please choose a username!';
    }

    if ( ! filter_var($email , FILTER_VALIDATE_EMAIL )) {
        $errors['email'] = 'Please enter a valid email address!';
    }
    if ($email === '') {
        $errors['email'] = 'Please enter your email-address!';
    }
    if ($password === '') {
        $errors['password'] = 'Please enter a password!';
    }

    if (strlen($password) < 8) {
        $errors['password'] = "Password too short!";
    }

    if (!preg_match("#[0-9]+#", $password)) {
        $errors['password'] = "Password must include at least one number!";
    }

    if (!preg_match("#[A-Z]+#", $password)) {
        $errors['password'] = "Password must include at least one capital!";
    } 
    
    // if (!preg_match("(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" , $password)) {
    //     $errors['password'] = "Password must include at least one special characters";
    // }

    if ($password !== $password_confirmation) {
        $errors['password'] = 'The passwords do not match!';
    }

    if(!isset($artist_category)) {
        $errors['artist_category'] = 'Please set a Category';
    }
    
    return $errors;

}

/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>

<main>
    <div class="form-container">
        <div>
            <form action="<?= url('auth/register.php') ?>" method="post">
                <h1>Create new account</h1>
                <!-- username -->  
                <div class="input-field">
                    <label class="require" for="title">Username</label>
                    <?php if (isset($errors['name']) ) : ?>
                        <div class="error">
                            <?= $errors['name']?>
                        </div>
                    <?php endif; ?>
                    <input type="text" name="name" id="name" required>
                </div>
                <!-- email -->
                <div class="input-field">
                    <label class="require" for="title">Email</label>
                    <?php if (isset($errors['email']) ) : ?>
                        <div class="error">
                            <?= $errors['email']?>
                        </div>
                    <?php endif; ?>
                    <input type="text" name="email" id="email" required>
                </div>

                <!-- artist category -->
                <div class="input-field">
                    <p class="require">Category</p>
                    <?php if (isset($errors['password']) ) : ?>
                        <div class="error">
                            <?= $errors['artist_category']?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($errors['artist_category']) ) : ?>
                        <div class="error">
                            <?= $errors['artist_category']?>
                        </div>
                    <?php endif; ?>
                    <label for="title">Animation</label>
                    <input type="radio" name="artist_category" id="animation" value="Animation" required>
                    <label for="title">Design</label>
                    <input type="radio" name="artist_category" id="design" value="Design" required>
                    <label for="title">Interactive</label>
                    <input type="radio" name="artist_category" id="interactive" value="Interactive" required>
                </div>
                <div class="input-field">
                    <label class="require"  for="title">Password</label>
                    <?php if (isset($errors['password']) ) : ?>
                        <div class="error">
                            <?= $errors['password']?>
                        </div>
                    <?php endif; ?>
                    <input type="text" name="password" id="password">
                </div>
                <!-- password confirmation-->
                <div class="input-field">
                    <label class="require" for="title">Password Confirmation</label>
                    <?php if (isset($errors['name'])) : ?>
                        <div class="bg-red-200">
                            <?= $errors['password_confirmation'] ?>
                        </div>
                    <?php endif; ?>
                    <input type="text" name="password_confirmation" id="password_confirmation">
                </div>
                <!-- formular abschicken-->
                <button type="submit">Sign up!</button>
            </form>
        </div>
    </div>
</main>
