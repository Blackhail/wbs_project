<?php declare(strict_types=1);

require_once '../../bootstrap.php';
$site_name = 'Logout';

$sql_profile = "SELECT `id` FROM `users`";
$profile_pages = db_raw_select($sql_profile);


logout();

redirect('index.php');
