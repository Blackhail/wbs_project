<?php declare( strict_types = 1 );

$site_name = 'grimm berlin';

require_once '../bootstrap.php';

$sql_profile = "SELECT `id` ,  `name` , `description` , `avatar` , `website` , `specials` FROM `users`";
$profiles = db_raw_select($sql_profile);

$sql_media = "SELECT  `url` , `user_id` , `header_image` , `project_id` FROM `media`";
$medias = db_raw_select($sql_media);

$sql_project = "SELECT `name`  , `user_id` , `id` FROM `project`";
$projects = db_raw_select($sql_project);

/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>


<main id="start-page">
	<div class="slider">
    <!-- später aus db -->
        <div class="inner">
            <?php foreach ($medias as $media) : ?>
                    <?php if ($media['header_image']) : ?>   
                        <img src="<?= $media['url'] ?>" alt="project header img">
                    <?php endif; ?>
            <?php endforeach; ?> 
            
        </div>
    </div>
    <div class="container-medium">
        <div class="start-info">
            <h1>GRIMM BERLIN STUDIO COLLECTIVE</h1>
            <p>
            We are a collective of award-winning animation filmmakers, united as Studio Grimm Berlin since 2009. We produce short films, music videos, animated documentaries, animation for advertisement, explanatory movies. Our styles range from artistic 3D, over cutout and 2D drawn animation to analogue stop motion under the camera.
            </p>
            <p>
            Grimm Berlin’ high end 3D department founded Lumatic in 2015.
            </p>
            <p>
            According to your needs we can handle the entire implementation of projects from the concept/script to the final product, or take care of certain aspects of the production. Our team is well established and in addition we have access to a large network of external partners we can draw on if necessary.
            </p>
        </div>
    </div>
</main>


<!--*************************** IMG - SLIDER ****************************-->

<script>
	(function(){
	    let left = 0
	    let cont = 1
	    const imagesToShow = parseInt($(".inner *").length)
	    setInterval(function() {
            if (cont++ <= imagesToShow){
                $(".inner").css("left",`${left}%`);
                left -= 100.3;
                } else{
                cont = 1
                left = 0
                }
	    },3000)
	})()
</script>

<?php
    include PATH.'parts/footer.php';