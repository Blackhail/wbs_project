<?php declare( strict_types = 1 );

require_once '../bootstrap.php';

$id = (int) query('id');

$get_project_id = $_GET['project_id'] ?? NULL;
$get_user_id = auth_id();

/// CHANGE DB CONTENTS ///////////////////////////////////////////////////////////////

if (request_is('post')) {
    switch (request('action')) :
        
        case 'upload_img':

            $file = $_FILES['url'];
            $fileName = $_FILES['url']['name'];
            $fileSize = $_FILES['url']['size'];
            $fileType = $_FILES['url']['type'];
            $fileTmpName = $_FILES['url']['tmp_name'];
            $fileError = $_FILES['url']['error'];
            $fileExt = explode('.', $fileName);
            $fileActualExt = strtolower(end($fileExt));
            $allowed = array('jpg', 'jpeg', 'png', 'gif');

            if (in_array($fileActualExt, $allowed)){

                if ($fileError === 0) {

                    if($fileSize < 5000000) {

                        $sql = "SELECT `id` FROM `media` ORDER BY `id` DESC LIMIT 1";
                        $new_media_id = db_raw_select($sql);

                        $fileNameNew =  $new_media_id[0]['id'] . '_img'.'.'.$fileActualExt;
                        $fileDesitination = '../images/'.$fileNameNew;
                        $url = $fileDesitination;
                        move_uploaded_file($fileTmpName, $fileDesitination);

                    } else {
                        $errors['url'] = "The file exceeds the maximum size!";
                    }
                } else {
                    $errors['url'] = "An error occurred during upload!";
                }
                } else {
                    $errors['avatar'] = "This file type is not allowed!";
                }
                
            if (!$errors) {
            db_insert('media', [
                'url' => '../images/'. $fileNameNew,
                'user_id' => $get_user_id,
                'project_id' => $get_project_id
                ]); 
            }      
        break;

        case 'delete_img':

            if (auth_id()) {
                db_delete('media', (int) request('id'));  
                redirect('project_page.php?project_id='. $get_project_id);
            }
             
        break;

    endswitch;
}

/// SQL ABFRAGEN /////////////////////////////////////////////////////////////////////

/// PROFILE
$sql_profile = "SELECT `id` ,  `name` , `specials` FROM `users` ";
$profiles = db_raw_select($sql_profile);
/// MEDIA
$sql_media = "SELECT  `url` , `user_id` , `header_image` , `id` , `project_id` FROM `media` ";
$medias = db_raw_select($sql_media);
/// PROJECT
$sql_project = "SELECT `name`  , `user_id` , `id` , `description` FROM `project` ";
$projects = db_raw_select($sql_project);

/// SITENAME //////////////////////////////////////////////////////////////////////////

foreach ($projects as $project) {
    if ($get_project_id === $project['id']) {
        $site_name = $project['name'];
    }
}

/// HTML //////////////////////////////////////////////////////////////////////////////

include PATH.'parts/head.php'; ?>

<main id="project-page">
    <!-- FALLBACK URL MANIPULATION -->
    <?php if (!$get_project_id) : ?>
        <h1 class="no-auth">You have no permission for this!</h1>
    <?php endif; ?>
    <!-- header-img -->
    <div class="img-container">
        <div class="main-img">
            <?php foreach ($medias as $media) : ?>
                <?php if ($media['project_id'] == $get_project_id && $media['header_image']) : ?>
                <figure>
                    <img src="<?= $media['url'] ?>" alt="project header img">
                </figure>
                <?php endif;?>
            <?php endforeach; ?> 
        </div>
    </div>
    <div class="container-medium clear">
        <div class="profile-info clear">
            <!-- PROJECT INFO LEFT -->
            <div class="profile-left">
                <div>           
                    <?php foreach ( $projects as $project ) {
                        if ( $project['id'] === $get_project_id ) {
                            foreach ($profiles as $profile ) {
                                if ( $project['user_id'] === $profile['id']) {?>
                                    <h1><?= $project['name'] ?></h1>
                                    <h2><?=$profile['name']?></h2>
                                    <p><?=$profile['specials']?></p>
                                <?php
                                }
                            }
                        }
                    }
                    ?>
                </div>
            </div>
            <!-- PROJECT INFO RIGHT -->
            <div class="profile-right">
                <?php foreach ( $projects as $project ) {
                 if ( $project['id'] === $get_project_id ) : ?>
                    <p><?= $project['description'] ?></p>
                <?php endif; } ?>
            </div>
        </div>
        <!-- IMG-CONTAINER -->
        <div class="img-container">
            <?php foreach ($projects as $project) {
             if (auth_id() === $project['user_id'] && $project['id'] === $get_project_id) {
                     ?>
                    <form action="project_page.php?project_id=<?= $project['id'] ?>" method="POST" enctype="multipart/form-data">
                        <label class="require" for="url">upload new general project image</label>
                        <input type="file" name="url" id="url">
                        <button type="submit" name="action" value="upload_img">upload image</button>
                    </form>
                    <?php
                }
            } ?>
            <div class="project-images">           
                <?php foreach ($medias as $media) {
                    if ($media['project_id'] === $get_project_id && !$media['header_image']) { ?>
                        <figure>
                            <img src="<?= $media['url'] ?>">
                        </figure>
                        <?php foreach ($projects as $project) {
                         if ($project['user_id']  === auth_id() && $project['id'] === $media['project_id']) : ?>
                            <form action="project_page.php?project_id=<?= $project['id'] ?>" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?= $media['id'] ?>">
                                <button type="submit" name="action" value="delete_img">delete image</button>
                            </form>
                        <?php endif;}
                    }
                } ?>    
            </div>
        </div>
    </div>
</main>
<?php
include PATH.'parts/footer.php';