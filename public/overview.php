<?php declare( strict_types = 1 );

// $site_name = 'overview'; /* hier später generierter artist name*/


require_once '../bootstrap.php';

///// SQL ABFRAGEN ///////////////////////////////////////////////////////////////////////////

$sql = "SELECT `id` , `name` , `artist_category` , `avatar` , `admin` FROM `users`";
$profiles = db_raw_select($sql);

// $sql = "SELECT * FROM `project` , `media` WHERE  `media`.`project_id` = `project`.`id` ";
$sql = "SELECT * FROM `project` , `media` WHERE  `media`.`project_id` = `project`.`id` ";
$projects = db_raw_select($sql);

///// GET VARIABLEN ///////////////////////////////////////////////////////////////////////////

$get_select = $_GET['select'] ?? NULL;
$get_subselect = $_GET['subselect'] ?? NULL;
if ($get_select === 'Artists') { 
    $site_name = $get_select;
}
if ($get_select === 'Animation') { 
    $site_name = $get_select;
}
if ($get_select === 'Design') { 
    $site_name = $get_select;
}
if ($get_select === 'Interactive') { 
    $site_name = $get_select;
}

/*************************** HTML ****************************/

include PATH.'parts/head.php'; ?>

<script src="<?= url('../scripts/overview.js') ?>"></script>

<main class="card-box">
<!-- HEADLINES & LINK TO ALL PROJECTS -->

<?php if ($get_select) : ?>
    <h1><?=$get_select?></h1>
        <?php if ($get_select !== 'Artists') : ?>
        <div class="all-project-link">
            <a href="overview.php">All Projects</a>
        </div>
        <?php endif;
    endif;
    
if ($get_subselect ?? NULL) : ?>
    <h1>Artists</h1>
<?php endif; 

if (!$get_select ==='Animation' || !$get_select ==='Design' || !$get_select ==='Interactive') : ?>
    <h1>All Projects</h1>
<?php endif;

if (!$get_select && !$get_subselect) : ?>
    <h1>All Projects</h1>
    <ul>
        <li><a href="<?= url('overview.php?select=Animation') ?> " title="Animation">Animation</a></li>
        <li><a href="<?= url('overview.php?select=Design') ?> "title="Design">Design</a></li>
        <li><a href="<?= url('overview.php?select=Interactive') ?> " title="Interactive">Interactive</a></li>
    </ul>
<?php endif; ?>

    <!-- FILTER FOR ARTISTS CATEGORY -->
    <div id="subcat">    
        <?php
        if ( $get_select == 'Artists' || $get_subselect == 'Animation' || $get_subselect == 'Design' ||$get_subselect == 'Interactive' ) { ?>
         
            <ul>
                <li><a href="<?= url('overview.php?subselect=Animation') ?> " title="Animation">Animation</a></li>
                <li><a href="<?= url('overview.php?subselect=Design') ?> "title="Design">Design</a></li>
                <li><a href="<?= url('overview.php?subselect=Interactive') ?> " title="Interactive">Interactive</a></li>
            </ul>
        <?php
        }
        ?>
    </div>
    <div class="cards clear">
        <!-- NAV ARTISTS -->
        <?php
        foreach ($profiles as $profile) {
            if ($get_select === 'Artists' && $profile['name'] !== 'admin') { 
               
            ?>
                <div class="card">
                    <a href="profile_page.php?id=<?= $profile['id']?>">
                        <figure>
                            <?php if($profile['avatar'] === '') : ?>
                                <img src="<?= url('../images/avatars/default.jpg')?>" alt="profile-img">
                            <?php endif; ?>
                            <img src="<?= $profile['avatar'] ?>" alt="profile-img">
                            <figcaption><?= $profile['name'] ?></figcaption>
                        </figure>
                    </a>
                </div>             
            <?php
            }
        }
        ?>
        <!-- FILTER ARTISTS CATEGORY -->
        <?php 
        foreach ($profiles as $profile) { 
            if ($get_subselect === $profile['artist_category'] && !$profile['admin']) {
            ?>
                <div id="subcat" class="card">
                    <a href="profile_page.php?id=<?= $profile['id']?>">
                        <figure>
                            <img src="<?= $profile['avatar'] ?>" alt="profile-img">
                            <figcaption><?= $profile['name'] ?></figcaption>
                        </figure>
                    </a>
                </div>          
            <?php
            }
        }
        ?>
        <!-- NAV ANIMATION, DESIGN, INTERACTIVE  -->
        <?php
        foreach ($projects as $project) {    
            if ($get_select === $project['project_category']) { 
                if ($project['header_image'] && $projects[1]['url']) { 
                ?>
                    <div class="card project-card">
                        <a href="project_page.php?project_id=<?= $project['project_id']?>">
                            <figure>
                                <img src="<?= $project['url'] ?>" alt="project-img">
                                <figcaption><?= $project['name'] ?></figcaption>
                            </figure>
                        </a>
                    </div>
                <?php
                }
            }
            
            if ( !$get_select && !$get_subselect ) { 
                if ($project['header_image']) { 
                ?>
                    <div class="card project-card">
                        <a href="project_page.php?project_id=<?= $project['project_id']?>">
                            <figure>
                                <img src="<?= $project['url'] ?>" alt="project-img">
                                <figcaption><?= $project['name'] ?></figcaption>
                            </figure>
                        </a>
                    </div>
                <?php
                }
            }

        }
        ?>
    </div>
</main>
<?php
include PATH.'parts/footer.php';