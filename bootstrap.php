<?php declare(strict_types=1);

const DEBUG = false;

const PATH = 'C:/xampp/htdocs/projects_on_git/wbs_project/';

const APP_DOMAIN = 'localhost';
const APP_BASE_URL = '/projects_on_git/wbs_project/public';

foreach ([
    'helpers',
    'request',
    'session',
    'authentication',
    'database_classic',
    'view',
    'grimm_project_specials'
] as $module) {
    require_once PATH."lib/php/$module.php";
}

$database = db_connect([
    'host' => 'localhost',
    'username' => 'root',
    'password' => '',
    'database' => 'grimm'
]);

session_start();

$errors = [];
