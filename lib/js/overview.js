(function (window, document) {
  /* MAGIC: jQuery oder doch kein jQuery!? */
  var $ = document.querySelector.bind(document);
  var $$ = document.querySelectorAll.bind(document);

  function changeArrow(pointer) {
    if (pointer.nextElementSibling.classList.contains('hide')) {
      pointer.getElementsByTagName('span')[0].classList.remove('down');
      pointer.getElementsByTagName('span')[0].classList.add('up');
    } else {
      pointer.getElementsByTagName('span')[0].classList.remove('up');
      pointer.getElementsByTagName('span')[0].classList.add('down');
    }
  }

  function toggleVisibility(pointer) {
    pointer.nextElementSibling.classList.toggle('hide');
  }

  function handleClick() {
    changeArrow(this);
    toggleVisibility(this);
  }

  window.addEventListener('load', function (e) {
    $$('.numberlist ol').forEach(function (element) {
      element.classList.add('hide');
    });

    $$('.numberlist h2').forEach(function (element) {
      element.addEventListener('click', handleClick);
    });
  });
})(window, document);
