<?php declare(strict_types=1);


function uploadAvatar() {
 
    $file = $_FILES['avatar'];
    $fileName = $_FILES['avatar']['name'];
    $fileSize = $_FILES['avatar']['size'];
    $fileType = $_FILES['avatar']['type'];
    $fileTmpName = $_FILES['avatar']['tmp_name'];
    $fileError = $_FILES['avatar']['error'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed = array('jpg', 'jpeg', 'png', 'gif');

    if (in_array($fileActualExt, $allowed)){

        if ($fileError === 0) {

            if($fileSize < 500000) {

            $fileNameNew = uniqid('', true).".".$fileActualExt;
            $fileDesitination = '../../images/avatars/'.$fileNameNew;
            move_uploaded_file($fileTmpName, $fileDesitination);
            header("Location: index.php?uploadsuccess");
            } else {
                echo "The file exceeds the maximum size!";
            }
        } else {
            echo "An error occurred during upload!";
        }
    } else {
        echo "This file type is not allowed!";
    }
}