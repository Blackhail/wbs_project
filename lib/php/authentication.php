<?php declare(strict_types=1);

/**
 * Dependencies:
 *  - session
 *  - db (for refresh_auth_user())
 */

function login($user)
{
    session('_user', $user);
}


function logout()
{
    session('_user', null);
}


function auth_user(string $key = null)
{
    if ($key) {
        return session('_user')[$key] ?? null;
    }

    return session('_user');
}


function auth_id()
{
    return auth_user('id');
}


function auth_refresh_user()
{
    if ($user_id = auth_id()) {
        login(db_raw_first('SELECT * FROM `users` WHERE `id` = ' . $user_id));
    }
}
