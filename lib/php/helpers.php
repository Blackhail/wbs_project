<?php declare(strict_types=1);


function array_extract(array $array, $key = null)
{
    if ($key === null) {
        return $array;
    }

    if (is_string($key)) {
        return $array[$key] ?? null;
    }

    if (is_array($key)) {
        return array_intersect_key($array, array_flip($key));
    }

    trigger_error('If you provide a key, it must be of type string or an array of strings. Please check the documentation.', E_USER_WARNING);
}
