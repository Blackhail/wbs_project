<?php declare(strict_types=1);

/**
 * Dependencies:
 *  - request
 *  - session
 *  - send_response
 */


const STATUS_BAD_REQUEST = 400;
const STATUS_NOT_FOUND = 404;
const STATUS_UNAUTHORIZED = 401;
const STATUS_METHOD_NOT_ALLOWED = 405;


function app_load_config(string $path)
{
    if (!file_exists($path)) {
        trigger_error("Your App config file \"$path\" seems to be missing.", E_USER_ERROR);
    }

    require $path;
}


function app_load_modules(string $module_path, array $modules)
{
    foreach ($modules as $module) {
        $file = "{$module_path}{$module}.php";

        if (!file_exists($file)) {
            trigger_error("Cannot find module file '$file'. Have you misspelled its name?", E_USER_ERROR);
        }

        require_once $file;
    }
}

// TODO: remove this / it's stupid
function app_cleanup()
{
    flash_rotate();

    db_disconnect();
}


function app_handle_error(int $error, $data = [])
{
    switch ($error) :
        case STATUS_BAD_REQUEST:
            if (request_wants_json()) {
                send_response([
                    'status' => 'Bad Request',
                    'errors' => $data
                ], STATUS_BAD_REQUEST);

            } else {
                flash('errors', $data);
                back();
            }
            break;

        case STATUS_UNAUTHORIZED:
            $response = request_wants_json()
                ? send_response(['Unauthorized'], STATUS_UNAUTHORIZED)
                : redirect(REDIRECT_IF_UNAUTHORIZED ?? 'login');
            break;

        case STATUS_NOT_FOUND:
            $response = request_wants_json()
                ? send_response(['status' => 'Not Found'], STATUS_NOT_FOUND)
                : send_response("Not Found", STATUS_NOT_FOUND);
            break;
    endswitch;

    app_cleanup();

    exit();
}
