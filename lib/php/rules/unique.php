<?php declare(strict_types=1);

function validate_unique($value, string $table, string $column)
{
    $result = db_raw_first("SELECT 1 FROM `$table` WHERE `$column` = " . db_prepare($value));

    if ($result) {
        return 'This :field: already exists in the database.';
    }
}
