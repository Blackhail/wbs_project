<?php declare(strict_types=1);

function validate_required($value)
{
    if ($value === '' || $value === null) {
        return 'The :field: field is required.';
    }
}