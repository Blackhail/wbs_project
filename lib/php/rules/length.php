<?php declare(strict_types=1);

function validate_length($value, $min_length, $max_length = null)
{
    if ($min_length && mb_strlen($value) < $min_length) {
        return "\":field:\" must be at least $min_length characters long.";
    }

    if ($max_length && mb_strlen($value) > $max_length) {
        return "\":field:\" cannot be longer than $max_length characters.";
    }
}
