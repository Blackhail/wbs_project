<?php declare(strict_types=1);

function validate_min($value, $minimum)
{
    if ((int) $value < (int) $minimum) {
        return "\":field:\" must be greater than $minimum.";
    }
}