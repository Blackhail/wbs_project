<?php declare(strict_types=1);

function validate_exists($value, string $table, string $column)
{
    $result = db_raw_first("SELECT 1 FROM `$table` WHERE `$column` = " . db_prepare($value));

    if (!$result) {
        return 'This :field: doesn\'t exist in the database.';
    }
}
