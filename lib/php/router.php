<?php declare(strict_types=1);

/**
 * Dependencies:
 *  - request (only for a single request_is() -> do I really need this?)
 */


function router(array $config)
{
    // TODO: Statt ? könnte auch # stehen!
    $path = explode('?', $_SERVER['REQUEST_URI'])[0];

    $path = substr($path, strlen(APP_BASE_URL));

    foreach ($config as $route_part => $action_part) {

        list($method, $route) = explode('::', $route_part);

        if (request_is($method)) {

            $regex = preg_replace('#{\w+?}#', '\w+' , $route);

            if (preg_match("#^$regex$#", $path)) {
                list($controller, $action) = explode('@', $action_part);

                $options = explode(':', $action);
                $function = $options[0];

                if (isset($options[1])) {
                    $options = explode(',', $options[1]);
                }

                if (!in_array('guest', $options) && !auth_user()) {
                    app_handle_error(STATUS_UNAUTHORIZED);
                }

                $file = PATH.'app/controllers/'.$controller.'_controller.php';

                if (!file_exists($file)) {
                    trigger_error("Cannot find controller file: $file", E_USER_ERROR);
                }

                require $file;

                if ($index = strrpos($controller, '/') !== false) {
                    $controller = substr($controller,  + 1);
                }

                $function = $controller.'_'.$function;

                if (!function_exists($function)) {
                    trigger_error("Cannot find controller function: $function ;", E_USER_ERROR);
                }

                $params = router_map_wildcards(
                    explode('/', $path),         // The path_segments
                    explode('/', $route)         // The route_segments
                );

                return call_user_func_array($function, $params);
            }
        }
    }

    app_handle_error(STATUS_NOT_FOUND);
}


function router_map_wildcards(array $route_segments) : array
{
    $params = [];

    foreach ($route_segments as $route_segment_index => $route_segment) {

        if (strpos($route_segment, '{') === 0) {
            $param = $path_segments[$route_segment_index];
            $name = substr($route_segment, 1, strlen($route_segment) - 2);

            $params[$name] = is_numeric($param) ? (int) $param : $param;
        }
    }

    return $params;
}