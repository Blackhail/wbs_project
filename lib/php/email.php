<?php declare(strict_types=1);

function validate_email($value)
{
    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
        return 'Please enter a valid email address.';
    }
}
