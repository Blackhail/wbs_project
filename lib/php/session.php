<?php declare(strict_types=1);

/**
 * Dependencies: none
 */

function session(string $key = null, $value = null)
{
    if (session_status() !== PHP_SESSION_ACTIVE) {
        trigger_error('You have to call session_start() in your code before you can use the session module.');
    }

    if ($key === null) {
        return $_SESSION;
    }

    if ($value === null && func_num_args() === 2) {
        unset($_SESSION[$key]);
    }

    if ($value !== null && func_num_args() === 2) {
        return $_SESSION[$key] = $value;
    }

    return $_SESSION[$key] ?? $_SESSION['_flash'][$key] ?? null;
}


function flash($key, $message)
{
    $flash_new = session('_flash_new');

    if (is_null($flash_new)) {
        trigger_error("To use the flash() helper, you must call flash_rotate() at the end of every request.", E_USER_WARNING);
    }

    $new_flash = session('_flash_new') ?? [];
    $new_flash[$key] =  $message;

    session('_flash_new', $new_flash);
}


function flash_rotate()
{
    flash('old', $_POST);

    session('_flash', session('_flash_new'));
    session('_flash_new', []);
}


function old($key) {
    return session('_flash')['old'][$key] ?? null;
}
