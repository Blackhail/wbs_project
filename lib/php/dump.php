<?php declare(strict_types=1);

/**
 * Dependencies: none
 */


function dump(...$args)
{
?>
    <table style="border: 1px solid black; margin: 2em auto;">
        <tr>
            <th>#</th>
            <th>Function</th>
            <th>File and Line</th>
        </tr>
        <?php
            $frames = debug_backtrace();
            $frame_count = count($frames);

            foreach ($frames as $index => $frame) {
            $file = substr($frame['file'], strlen(PATH));
        ?>
            <tr <?= $index % 2 ? 'style="background: LightGray"' : '' ?>>
                <td style="padding-right: 1em"><?= $frame_count - $index - 1 ?>.</td>
                <td style="padding-right: 3em"><?= $frame['function'] ?>()</td>
                <td><?= "$file : {$frame['line']}" ?></td>
            </tr>
        <?php } ?>
    </table>
    <pre>
        <?php var_dump(...$args); ?>
    </pre>
<?php }


function dd(...$args)
{
    dump(...$args);
    die();
}