<?php declare(strict_types=1);

/**
 * Dependencies: none
 */

function db_connect(array $config) : mysqli
{
    $db = mysqli_connect(
        $config['host'],
        $config['username'],
        $config['password'],
        $config['database'],
        $config['port'] ?? 3306
    );

    mysqli_set_charset($db, $config['charset'] ?? 'utf8mb4');
    
    if (mysqli_connect_errno()) {
        trigger_error("DB Error: " . mysqli_connect_error(), E_USER_ERROR);
    }

    return $db;
}


function db_disconnect()
{
    global $database;
    db_check_global($database);

    return mysqli_close($database);
}


function db_raw_select($sql) : array
{
    $result = db_query($sql);

    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_free_result($result);

    return $data;
}


function db_raw_first($sql) : array
{
    return db_raw_select($sql . ' LIMIT 1')[0] ?? [];
}


function db_insert(string $table, array $data)
{
    global $database;
    db_check_global($database);
    
    $columns = [];
    $values = [];

    foreach ($data as $column => $value) {
        $columns[] = "`$column`";
        $values[] = db_prepare($value);
    }

    $columns = implode(', ', $columns);
    $values = implode(', ', $values);

    $sql = "INSERT INTO `$table` ($columns) VALUES ($values)";
 
    db_query($sql);

    return mysqli_insert_id($database) ?? false;
}


function db_update(string $table, int $id, array $data)
{
 
    global $database;
    db_check_global($database);

    $pairs = [];

    foreach ($data as $column => $value) {
        $pairs[] = "`$column` = " . db_prepare($value);
    }

    $pairs = implode(', ', $pairs);

    $sql = "UPDATE `$table` SET $pairs WHERE `id` = $id";
  
    db_query($sql);

    return mysqli_affected_rows($database);
}


function db_delete(string $table, $id)
{
    global $database;
    db_check_global($database);
    
    $sql = "DELETE FROM `$table` WHERE `id` IN (" . implode(', ', (array) $id) . ")";

    db_query($sql);
    
    return mysqli_affected_rows($database);
}


function db_query(string $sql)
{
    global $database;
    db_check_global($database);

    $result = mysqli_query($database, $sql);

    if (mysqli_errno($database)) {
        trigger_error("DB Error : " . mysqli_error($database), E_USER_ERROR);
    }

    // TODO: Maybe turn this into a hook call to app_log
    if (defined('DEBUG') && DEBUG) {
        $path = PATH ?? '';
        error_log(
            date('Y/m/d H:m:s')
            . ' SQL: '
            . str_replace(PHP_EOL, " ", $sql)
            . PHP_EOL, 3, $path
            . "framework.log"
        );
    }

    return $result;
}


function db_prepare($value) : string
{
    global $database;
    db_check_global($database);
    
    if (is_bool($value)) {
        return $value ? '1' : '0';
    }

    if (is_string($value)) {
        return "'" . mysqli_escape_string($database, $value) . "'";
    }

    return (string) $value;
}


function db_check_global($database)
{
    if (!isset($database) || ! $database instanceof mysqli) {
        trigger_error('The db-library requires a globally defined variable called $database that holds a MySQL/MariaDB connection (mysqli-Object). You can use db_connect() to establish this connection;', E_USER_ERROR);
    }
}