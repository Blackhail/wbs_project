<?php declare(strict_types=1);

/**
 * Requires
 *  - request
 *  - session
 */

function validate(array $data, array $config)
{
    $validated_data = [];
    $errors = [];

    foreach ($config as $field => $rules) {

        if (is_string($rules)) {
            $rules = explode('|', $rules);
        }

        $value = $data[$field] ?? null;

        if ($value === null) {
            if (in_array('required', $rules)) {
                $rules = ['required'];

            } else {
                continue;
            }
        }

        foreach ($rules as $rule) {

            if (is_callable($rule)) {
                $function = $rule;

            } else {
                [$rule, $arguments] = validation_get_rule_and_arguments($rule);

                validation_load_rule($rule);

                $function = "validate_$rule";
            }

            $error_message = $function($value, ...$arguments);

            if ($error_message) {
                $errors[$field][] = str_replace(':field:', $field, $error_message);
            }

            if (empty($errors[$field])) {
                $validated_data[$field] = $value;
            }
        }
    }

    if ($errors) {
        app_handle_error(STATUS_BAD_REQUEST, $errors);

    } else {
        return $validated_data;
    }
}


function validation_load_rule(string $rule)
{
    if (
        !file_exists($file = PATH."lib/rules/$rule.php") &&
        !file_exists($file = PATH."app/rules/$rule.php")
    ) {
        trigger_error("Cannot find rule file for $rule.");
    }

    require_once($file);
}


function validation_get_rule_and_arguments($rule)
{
    $arguments = [];

    if (strpos($rule, ":")) {
        $rule = explode(":", $rule);

        $rule_name = $rule[0];

        array_shift($rule);
        $arguments = $rule;
    }

    return [$rule_name ?? $rule, $arguments];
}


function errors_for(string $field) : array
{
    return session('_flash')['errors'][$field] ?? [];
}


function error_for(
    string $field,
    $form_string = true
) : string
{
    $error = errors_for($field)[0] ?? '';

    if ($form_string === false) {
        return $error;
    }

    if ($form_string === true) {
        if (!defined('ERROR_MESSAGE_FORM_STRING')) {
            trigger_error('Please set a constant named ERROR_MESSAGE_FORM_STRING. The substring "%s" will be replaced with the actual error message..');
        }

        $form_string = ERROR_MESSAGE_FORM_STRING;
    }

    return sprintf($form_string, $error);
}


function errors()
{
    return session('_flash')['errors'] ?? [];
}
