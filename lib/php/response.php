<?php declare(strict_types=1);

/**
 * Dependencies: none
 */


function send_response($data, int $status_code = 200)
{
    http_response_code($status_code);

    if (is_array($data)) {
        header('Content-Type: application/json');
        echo json_encode($data);

    } else {
        echo $data;
    }
}
