<?php declare(strict_types=1);

/**
 * Dependencies:
 *  - authentication
 *  - request
 *  - response
 *  - app for app_cleanup()
 */


function user_can(string $permission, string $policy, $entity = null) : bool
{
    $file = PATH."app/policies/{$policy}_policy.php";

    if (!file_exists($file)) {
        trigger_error("Cannot find policies for $policy in $file", E_USER_ERROR);
    }

    require_once $file;

    $permission = str_replace(' ', '_', strtolower($permission));

    $function = "can_{$permission}";

    return $function(auth_user(), $entity);
}


function authorize(string $permission, string $policy, array $entity = null)
{
    if (!user_can($permission, $policy, $entity)) {
        app_handle_error(STATUS_UNAUTHORIZED);
    }
}
