<?php declare(strict_types=1);

/**
 * Dependencies:
 *  - app for app_cleanup()
 */

function request_method() : string
{
    return $_SERVER['REQUEST_METHOD'];
}


function request_is(string $method) : bool
{
    return strtoupper(request_method()) === strtoupper($method);
}


function request(string $key = null)
{
    if (request_wants_json()) {
        $json = file_get_contents('php://input');
        $data = json_decode($json, true);

        return array_extract($data, $key);
    }

    return array_extract($_POST, $key);
}


function query($key = null)
{
    return array_extract($_GET, $key);
}


function url(string $path) : string
{
    if (!defined("APP_DOMAIN")) {
        trigger_error("You must create the constant APP_DOMAIN and set it to the FQDN of the app if you want to use the url() helper. eg. const APP_DOMAIN = 'mysite.de';");
    }

    if (!defined("APP_BASE_URL")) {
        trigger_error("You must create the constant APP_BASE_URL and set it to the URL base path of the app. eg. const APP_BASE_URL = '/kurs/php/forum';");
    }

    return
        $_SERVER['REQUEST_SCHEME']
        . '://'
        . APP_DOMAIN
        . APP_BASE_URL
        . '/'
        . $path;
}


function redirect(string $path, int $status_code = 302, bool $is_absolute = false)
{
    if (function_exists('app_cleanup')) {
        app_cleanup();
    }

    if (!$is_absolute) {
        $path = url($path);
    }

    http_response_code($status_code);
    header('Location: '. $path);

    exit();
}


function back(int $status_code = 302)
{
    $referer_host = parse_url($_SERVER['HTTP_REFERER'])['host'];

    if ($referer_host !== APP_DOMAIN) {
        trigger_error('Security warning. Cannot redirect back.');
    }

    redirect($_SERVER['HTTP_REFERER'] ?? url(''), $status_code, true);
}


function request_wants_json()
{
    return
        strpos($_SERVER['HTTP_ACCEPT'], '/json') ||
        strpos($_SERVER['HTTP_ACCEPT'], '+json');
}