<?php declare(strict_types=1);

/**
 * Dependencies: none
 */


function html_escape(string $string, string $encoding = 'UTF-8') : string
{
    if ($string === '') {
        return '';
    }

    return htmlspecialchars($string, ENT_QUOTES, $encoding);
}


function e($string) : string
{
    return html_escape((string) $string, 'UTF-8');
}


function view_template_path(string $template_name) : string
{
    return PATH.'views/'.$template_name.'.php';
}


function view(
    string $template_name,
    array $data = [],
    $layout = 'layout'
) : string
{
    global $view_meta_data;

    if (!isset($view_meta_data)) {
        trigger_error('$view_meta_data missing. If you want to use the view helpers, you must create this variable and initialize it to an array.', E_USER_ERROR);
    }

    $template = view_template_path($template_name);

    if (!file_exists($template)) {
        trigger_error("The template $template does not exist.", E_USER_ERROR);
    }

    extract($view_meta_data);

    extract($data);

    ob_start();

    if ($layout === false) {
        include $template;

    } else {

        $layout = view_template_path($layout);

        if (!file_exists($layout)) {
            trigger_error("The layout $layout does not exist.", E_USER_ERROR);
        }

        include $layout;
    }

    $html = ob_get_clean();

    return $html;
}


function view_add(string $key, $value)
{
    global $view_meta_data;

    if (!isset($view_meta_data)) {
        trigger_error('$view_meta_data missing. If you want to use the view helpers, you must create this variable and initialize it to an array.', E_USER_ERROR);
    }

    return $view_meta_data[$key] = $value;
}
